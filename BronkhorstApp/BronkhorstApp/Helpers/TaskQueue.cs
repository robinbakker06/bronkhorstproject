﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BronkhorstApp.Helpers
{
    /// <summary>
    /// Queue for tasks. Executes after each other. Uses a semaphore to release.  This goes not in order!
    /// </summary>
    public static class TaskQueue
    {
        public static SemaphoreSlim semaphore;

        private static List<Task> T_Queue { get; set; }

        /// <summary>
        /// Initializes the QUEUE with the amount of parallel tasks at once.
        /// </summary>
        /// <param name="amountParallelism">The amount parallelism.</param>
        public static void Initialize(int amountParallelism)
        {
            semaphore = new SemaphoreSlim(amountParallelism);
        }


        /// <summary>
        /// Enqueues the specified task generator.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="taskGenerator">The task generator.</param>
        /// <returns></returns>
        public static async Task<T> Enqueue<T>(Func<Task<T>> taskGenerator)
        {
            // Wait for the task to finish and return a result
            await semaphore.WaitAsync();
            try
            {
                var result = taskGenerator().Result;

                return result;
            }
            finally
            {

                // If there is a result then release and let the next one run
                semaphore.Release();
            }
        }
    }
}
