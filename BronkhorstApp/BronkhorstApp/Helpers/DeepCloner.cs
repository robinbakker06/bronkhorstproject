﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.Helpers
{
    /// <summary>
    /// Deepcloner class. Clones the memory stream of the object to clone a object.  (not used) only for JSON storage and comparing them.
    /// </summary>
    public static class DeepCloner
    {
        /// <summary>
        /// Deep clones the specified object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }
    }
}
