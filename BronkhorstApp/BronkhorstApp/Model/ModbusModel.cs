﻿using BronkhorstApp.Helpers;
using BronkhorstApp.Model.JSON;
using BronkhorstModbusLibrary.Communication;
using BronkhorstModbusLibrary.Communication.Modbus;
using BronkhorstModbusLibrary.Communication.Modbus.Bronkhorst;
using BronkhorstModbusLibrary.Enumerations;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.Model
{
    public class ModbusModel
    {

        /// <summary>
        /// Retrieves the existings configurations from the disk as JSON.
        /// </summary>
        /// <returns></returns>
        public List<ModbusSettings> RetrieveExistingsConfigurations()
        {
            return JSONModel.GetAllObjects<ModbusSettings>();
        }
    }
}
