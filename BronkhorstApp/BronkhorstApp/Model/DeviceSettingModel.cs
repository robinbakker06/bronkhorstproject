﻿using BronkhorstModbusLibrary.Communication;
using BronkhorstModbusLibrary.Communication.Modbus;
using BronkhorstModbusLibrary.Communication.Modbus.Bronkhorst;
using BronkhorstModbusLibrary.Devices;
using BronkhorstModbusLibrary.Enumerations;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.Model
{
    /// <summary>
    /// Devices settings model.
    /// </summary>
    public class DeviceSettingModel
    {

        /// <summary>
        /// Saves the device as JSON (not used)
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="comport">The comport.</param>
        /// <param name="baudrate">The baudrate.</param>
        /// <param name="parity">The parity.</param>
        /// <param name="devicetype">The devicetype.</param>
        /// <param name="id">The identifier.</param>
        public void SaveDevice(string name, int comport, int baudrate, string parity, string devicetype, int id)
        {
            Parity p;

            if (parity.ToLower() == "even")
            {
                p = Parity.Even;
            }
            else
            {
                p = Parity.Odd;
            }
                

            ModbusSettings settings = new ModbusSettings(name, comport, baudrate, ModbusType.RTU, p);

            if (BronkhhorstSetup.Devices == null)
                BronkhhorstSetup.AddDevice(name, id, BronkhorstDeviceType.EL_FLOW, settings);
        }
    }
}
