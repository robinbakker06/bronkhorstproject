﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.Model.JSON
{
    public static class JSONModel
    {
        public const string SAVE_LOCATION = @"C:/SaxionBronkhorst/DATA";

        public const string OBJECT_LOCATION = @"/Objects";

        public static void SaveList(object data)
        {

        }

        /// <summary>
        /// Saves the object.
        /// </summary>
        /// <param name="data">The object to save, this will be a map per object.</param>
        public static bool SaveObject<T>(object data)
        {
            CheckRequirements(data.GetType().Name);

            foreach (JsonFile<T> obj in GetAllFileObjects<T>())
            {
                string json2 = JsonConvert.SerializeObject(data, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.None
                    });

                if (obj.JSONFormat == json2)
                {
                    return false;
                }
            }

            string json = JsonConvert.SerializeObject(data, Formatting.Indented,
            new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.None
            });

            string fName = SAVE_LOCATION + OBJECT_LOCATION + "/" + data.GetType().Name + "/_" + UniqueNumber() + "_" + data.GetType().Name + ".json";
            File.WriteAllText(fName, json);

            return true;
        }

        /// <summary>
        /// Gets all Objects as files
        /// </summary>
        /// <typeparam name="T">Type parameter of object to retrieve</typeparam>
        /// <returns>List of all objects asked</returns>
        public static List<JsonFile<T>> GetAllFileObjects<T>()
        {
            string location = SAVE_LOCATION + OBJECT_LOCATION + "/" + typeof(T).Name;
            if (!Directory.Exists(location))
                return null;

            List<JsonFile<T>> savedTypes = new List<JsonFile<T>>();

            foreach (var file in Directory.GetFiles(location))
            {
                FileInfo fileInfo = new FileInfo(file);
                string json = File.ReadAllText(file);

                T jsonObject = JsonConvert.DeserializeObject<T>(json, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                JsonFile<T> jsonFile = new JsonFile<T>(jsonObject);

                jsonFile.FullPath = file;
                jsonFile.FileSettings = fileInfo;
                jsonFile.JSONFormat = json;

                savedTypes.Add(jsonFile);

            }

            return savedTypes;
        }

        /// <summary>
        /// Gets all objects.
        /// </summary>
        /// <typeparam name="T">Type parameter of object to retrieve</typeparam>
        /// <returns>Retrieves all objects asked.</returns>
        public static List<T> GetAllObjects<T>()
        {
            string location = SAVE_LOCATION + OBJECT_LOCATION + "/" + typeof(T).Name;

            CheckRequirements(typeof(T).Name);

            List<T> savedTypes = new List<T>();


            foreach (var file in Directory.GetFiles(location))
            {
                T jsonObject = JsonConvert.DeserializeObject<T>(File.ReadAllText(file), new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                savedTypes.Add(jsonObject);
            }

            return savedTypes;
        }

        public static bool ObjectExist<T>(T obj)
        {
            CheckRequirements(typeof(T).Name);

            string location = SAVE_LOCATION + OBJECT_LOCATION + "/" + typeof(T).Name;

            foreach (var file in Directory.GetFiles(location))
            {
                FileInfo fileInfo = new FileInfo(file);
                string json = File.ReadAllText(file);
                string json2 = JsonConvert.SerializeObject(obj, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.None
                    });

                if (json == json2)
                { return true; }
            }

            return false;
        }

        public static void RemoveObject<T>(T refObj)
        {

            CheckRequirements(refObj.GetType().Name);
            List<JsonFile<T>> jsonObjects = GetAllFileObjects<T>();

            foreach (JsonFile<T> obj in jsonObjects)
            {

                string json = JsonConvert.SerializeObject(obj.ObjectType, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.None
                    });
                string json2 = JsonConvert.SerializeObject(refObj, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.None
                    });


                if (json == json2)
                {
                    obj.Remove();
                    return;
                }
            }

        }

        /// <summary>
        /// Replaces the object given with the edited object
        /// </summary>
        /// <typeparam name="T">Type parameter of the object</typeparam>
        /// <param name="objOriginal">The Original object to replace</param>
        /// <param name="objChanged">The changed object to replace the original with</param>
        public static bool ReplaceObject<T>(T objOriginal, T objChanged)
        {
            CheckRequirements(objOriginal.GetType().Name);

            List<JsonFile<T>> jsonObjects = GetAllFileObjects<T>();

            foreach (JsonFile<T> obj in jsonObjects)
            {

                string json = JsonConvert.SerializeObject(obj.ObjectType, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.None
                    });
                string json2 = JsonConvert.SerializeObject(objOriginal, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.None
                    });


                if (json == json2)
                {
                    return obj.ReplaceObject(objChanged);
                }
            }

            return false;

        }

        /// <summary>
        /// Replaces the property in object (parameter in class)
        /// </summary>
        /// <typeparam name="T">The target type parameter where the property is located</typeparam>
        /// <typeparam name="X">The replacement property object to replace the property with</typeparam>
        /// <param name="refObj">The reference object.</param>
        /// <param name="propertyExpression">The property expression to get the name of the property</param>
        /// <param name="replacement">The replacement.</param>
        public static void ReplacePropertyInObjectWhere<T, X>(T refObj, Expression<Func<X>> propertyExpression, X replacement)
        {
            CheckRequirements(refObj.GetType().Name);

            Type tx = refObj.GetType();
            List<JsonFile<T>> jsonObjects = GetAllFileObjects<T>();

            foreach (JsonFile<T> o in jsonObjects)
            {
                string json = JsonConvert.SerializeObject(o.ObjectType, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.None
                    });
                string json2 = JsonConvert.SerializeObject(refObj, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.None
                    });

                if (json == json2)
                {
                    o.ReplaceParameter(GetPropertyName(propertyExpression), replacement);
                }
            }

        }

        public static void AddObjectToList<T, X>(T refObj, Expression<Func<List<X>>> listProperty, X addition)
        {
            CheckRequirements(refObj.GetType().Name);

            Type tx = refObj.GetType();
            List<JsonFile<T>> jsonObjects = GetAllFileObjects<T>();

            foreach (JsonFile<T> o in jsonObjects)
            {
                string json = JsonConvert.SerializeObject(o.ObjectType, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.None
                    });
                string json2 = JsonConvert.SerializeObject(refObj, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.None
                    });

                if (json == json2)
                {
                    o.AddToList(GetPropertyName(listProperty), addition);
                }
            }

        }

        private static int UniqueNumber()
        {
            Random rand = new Random(Guid.NewGuid().GetHashCode());

            return rand.Next(0, 99999);

        }

        private static void CheckRequirements(string dtFolder)
        {
            if (!Directory.Exists(SAVE_LOCATION))
            {
                Directory.CreateDirectory(SAVE_LOCATION);
            }

            if (!Directory.Exists(SAVE_LOCATION + OBJECT_LOCATION))
            {
                Directory.CreateDirectory(SAVE_LOCATION + OBJECT_LOCATION);
            }

            if (!Directory.Exists(SAVE_LOCATION + OBJECT_LOCATION + "/" + dtFolder))
            {
                Directory.CreateDirectory(SAVE_LOCATION + OBJECT_LOCATION + "/" + dtFolder);
            }
        }

        public static string GetPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            return (propertyExpression.Body as MemberExpression).Member.Name;
        }
    }
}
