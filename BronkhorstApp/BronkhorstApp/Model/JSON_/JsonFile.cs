﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;

namespace BronkhorstApp.Model.JSON
{
    public class JsonFile<T>
    {
        public string FullPath { get; set; }

        public T ObjectType { get; set; }

        public FileInfo FileSettings { get; set; }

        public string JSONFormat { get; set; }

        public JsonFile(T data)
        {
            this.ObjectType = data;
        }

        public bool ReplaceObject(T newObj)
        {
            try
            {                                
                this.ObjectType = newObj;

                File.Delete(FullPath);
                File.WriteAllText(FullPath, JsonConvert.SerializeObject(newObj, Formatting.Indented,
                new JsonSerializerSettings
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.None
                }));

                this.FileSettings = new FileInfo(FullPath);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public void ReplaceParameter(string parameter, object replacement)
        {
            JObject o = JObject.FromObject(ObjectType);

            IEnumerable<JToken> tokenvalues = o.SelectTokens("$.." + parameter);

            foreach (JToken t in tokenvalues)
            {
                t.Replace(JToken.FromObject(replacement));
            }

            File.WriteAllText(FullPath, JsonConvert.SerializeObject(o, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    }));

        }

        public void AddToList(string list, object addition)
        {
            JObject o = JObject.FromObject(ObjectType);

            JArray listAsArray = (JArray)o[list];

            JObject additionObject = JObject.Parse(JsonConvert.SerializeObject(addition, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.None
                    }));

            listAsArray.Add(additionObject);

            File.WriteAllText(FullPath, JsonConvert.SerializeObject(o, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.None
                    }));

        }

        public void Remove()
        {
            File.Delete(FullPath);
        }

        public void ReplaceObject(string parameter, string replacement, string propertyWhere, string whereValue)
        {
            //ToDO
        }
    }
}