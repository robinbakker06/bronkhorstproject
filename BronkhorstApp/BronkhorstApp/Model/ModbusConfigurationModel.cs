﻿using BronkhorstApp.Model.JSON;
using BronkhorstModbusLibrary.Communication.Modbus;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.Model
{
    public class ModbusConfigurationModel
    {
        public bool SaveSettings(string name, int comport, int baudrate, string parity, int id)
        {
            Parity p;

            if (parity.ToLower() == "even")
            {
                p = Parity.Even;
            }
            else
            {
                p = Parity.Odd;
            }


            ModbusSettings settings = new ModbusSettings(name, comport, baudrate, ModbusType.RTU, p);

            if (!JSONModel.ObjectExist(settings))
            {
                return JSONModel.SaveObject<ModbusSettings>(settings);
            }
            else
                return false;

        }

        /// <summary>
        /// Edits the modbus configuration
        /// </summary>
        /// <param name="name">The name of the configuration</param>
        /// <param name="comport">The comport.</param>
        /// <param name="baudrate">The baudrate.</param>
        /// <param name="parity">The parity.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public bool EditSettings(string name, int comport, int baudrate, string parity, int id)
        {
            Parity p;

            if (parity.ToLower() == "even")
            {
                p = Parity.Even;
            }
            else
            {
                p = Parity.Odd;
            }


            ModbusSettings settings = new ModbusSettings(name, comport, baudrate, ModbusType.RTU, p);

            ModbusSettings existingSetting = JSONModel.GetAllObjects<ModbusSettings>().Where(x => x.Name.ToLower() == name.ToLower()).FirstOrDefault();

            if (existingSetting != null)
            {
                return JSONModel.ReplaceObject<ModbusSettings>(existingSetting, settings);
            }

            return false;

        }
    }
}
