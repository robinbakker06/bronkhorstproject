﻿using BronkhorstApp.Model.JSON;
using BronkhorstModbusLibrary.Devices;
using BronkhorstModbusLibrary.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.Model
{
    public class MainModel
    {
        public List<BronkhorstDevice> RetrieveAllIQFlowModules()
        {
            return JSONModel.GetAllObjects<BronkhorstDevice>().Where(x => x.DeviceType == BronkhorstDeviceType.IQ_FLOW_MODULE).ToList();
        }
    }
}
