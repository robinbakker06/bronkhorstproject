﻿using BronkhorstApp.Helpers;
using BronkhorstApp.Model.JSON;
using BronkhorstModbusLibrary.Communication.Modbus.Bronkhorst;
using BronkhorstModbusLibrary.Devices;
using BronkhorstModbusLibrary.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace BronkhorstApp.Model
{
    internal delegate void SamplesReached(int sensorId);

    /// <summary>
    /// Model for the LiveView. Handles business logic.
    /// </summary>
    public class LiveModel
    {
        public event MeasureReceivedParallel MeasureReceivedEvent;

        internal static event SamplesReached SamplesReachedEvent;

        public static CancellationTokenSource MeasureTokenSource;
        public static CancellationToken MeasureToken;

        private BronkhorstDevice elFlow;
        List<BronkhorstDevice> devices;


        // Synchronised Run (if each sensor has 5 samples collected then it gets incremented)
        int SyncRun = 1;
        // StepHeight per run
        double stepHeight;
        //Begin flow SP
        double beginFlowSp;
        int totalSteps = 0;

        /// <summary>
        /// Starts the measure.
        /// </summary>
        /// <param name="beginFlowSp">The begin flow ssetpointp.</param>
        /// <param name="endFlowSp">The end flow setpointp.</param>
        /// <param name="settlingTime">The settling time.</param>
        /// <param name="devices">The IQ Flow devices to measure</param>
        /// <param name="measureTimeSec">The measure time seconds.</param>
        /// <param name="sampleRate">The sample rate.</param>
        /// <param name="measurePoints">The amount of measure points.</param>
        /// <exception cref="NullReferenceException">Throws if there are no devices.</exception>
        public void StartMeasure(double beginFlowSp, double endFlowSp, double settlingTime, List<BronkhorstDevice> devices, int measureTimeSec, int sampleRate, int measurePoints)
        {
            if (devices == null)
                throw new NullReferenceException("Device is null!");

            LiveModel.SamplesReachedEvent += LiveModel_SamplesReachedEvent;
            TaskQueue.Initialize(1);

            SensorsIDSamplesReached = new List<int>();

            MeasureTokenSource = new CancellationTokenSource();
            MeasureToken = MeasureTokenSource.Token;

            elFlow = JSONModel.GetAllObjects<BronkhorstDevice>().Where(x => x.DeviceType == BronkhorstDeviceType.EL_FLOW).First();
            elFlow.ModbusConnect();
            elFlow.WriteRegister(BronkhorstRegisters.GetRegister("Initreset", BronkhorstDeviceType.EL_FLOW), 64);
            elFlow.WriteRegister(BronkhorstRegisters.GetRegister("FSetpoint", BronkhorstDeviceType.EL_FLOW), (float)beginFlowSp);

            // Initialise the begin flow setpoint
            this.beginFlowSp = beginFlowSp;

            // Calculate the step height.
            stepHeight = (endFlowSp - beginFlowSp) / (measurePoints );

            totalSteps = Convert.ToInt16((endFlowSp - beginFlowSp) / stepHeight) + 1;

            #region TEST
            //elFlow.WriteRegister(BronkhorstRegisters.GetRegister("FSetpoint", BronkhorstDeviceType.EL_FLOW), (float)beginFlowSp);

            //while (true)
            //{
            //    Thread.Sleep(1000);
            //    foreach (BronkhorstDevice device in devices)
            //    {
            //        device.ModbusConnect();

            //        // device.WriteRegister(BronkhorstRegisters.GetRegister("Initreset", BronkhorstDeviceType.IQ_FLOW_MODULE), 64);

            //        // MessageBox.Show("Device: " + device.DeviceName + ". Capacity Unit: " + device.ReadRegister(BronkhorstRegisters.GetRegister("Capacity Unit", BronkhorstDeviceType.IQ_FLOW_MODULE)));



            //        //  MessageBox.Show("Device:EL Flow. MaxFlow: " + elFlow.ReadRegister(BronkhorstRegisters.GetRegister("Fluid Capacity", BronkhorstDeviceType.IQ_FLOW_MODULE)));
            //        // MessageBox.Show("Device: EL FLOW. Flow: " + elFlow.ReadRegister(BronkhorstRegisters.GetRegister("FMeasure", BronkhorstDeviceType.EL_FLOW)));
            //        // MessageBox.Show("Device: " + device.DeviceName + ". Flow: " + device.ReadRegister(BronkhorstRegisters.GetRegister("Sensor Type", BronkhorstDeviceType.IQ_FLOW_MODULE)));


            //        MessageBox.Show("Device: " + device.DeviceName + ". Flow: " + device.ReadRegister(BronkhorstRegisters.GetRegister("FMeasure", BronkhorstDeviceType.IQ_FLOW_MODULE)));
            //      //  MessageBox.Show("Device EL: " + device.DeviceName + ". Flow: " + device.ReadRegister(BronkhorstRegisters.GetRegister("FMeasure", BronkhorstDeviceType.EL_FLOW)));

            //    }
            //}
            #endregion

            // Global variable for all devices.
            this.devices = devices;

            List<Task> parallelTasks = new List<Task>();
            foreach (BronkhorstDevice device in devices)
            {
                parallelTasks.Add(new Task(() =>
                {
                    device.ModbusConnect();

                    // NOTE: Every time calculation in mSec

                    double stepAmountSampleRate = (measureTimeSec * 1000) / sampleRate;

                    DateTime dtStart = DateTime.Now;

                    int CurrentRun = 1;
                    while (true)
                    {
                        DateTime mSecBegin = DateTime.Now;


                        // If the user doesn't cancel, then check if the runs are synchronised.
                        // Synchronisation per run is when all the sensors have x samples collected.
                        if (!MeasureTokenSource.IsCancellationRequested &&
                            SyncRun == CurrentRun && CurrentRun <= totalSteps)
                        {
                            //Check if the settling time has been reached.
                            if ((settlingTime * 1000) <= (DateTime.Now - dtStart).TotalMilliseconds)
                            {
                                int amountSamples = 0;

                                #region Samples per measurepoint loop
                                while (true)
                                {
                                    bool samplesReached = false;
                                    Thread.Sleep(TimeSpan.FromMilliseconds(stepAmountSampleRate));


                                    if (amountSamples == sampleRate)
                                    {
                                        samplesReached = true;
                                    }
                                    else
                                    {

                                        //Create a task to read the IQ-Flow and the EL-Flow.
                                        Func<Task<object[]>> tsk = () => Task.Run(() =>
                                        {
                                            object[] retValue = new object[2];
                                            retValue[0] = device.ReadRegister(BronkhorstRegisters.GetRegister("FMeasure", BronkhorstDeviceType.IQ_FLOW_MODULE));
                                            retValue[1] = elFlow.ReadRegister(BronkhorstRegisters.GetRegister("FMeasure", BronkhorstDeviceType.EL_FLOW));

                                            return retValue;
                                        });

                                        // Queue the task and wait for it to process. Queuing this tasks is a way of
                                        // Eliminating a problem where two different sensors are addressed at the exact 
                                        // same moment.
                                        object[] retVal = TaskQueue.Enqueue(tsk).Result;

                                        if (retVal != null)
                                        {
                                            Application.Current.Dispatcher.Invoke(() =>    // Update the graph at the User Interface.
                                            {
                                                MeasureReceivedEvent?.Invoke(retVal[0], (DateTime.Now - dtStart).TotalMilliseconds, retVal[1], device.UnitIdentifier, (beginFlowSp + (stepHeight * ( CurrentRun - 1))));
                                            });
                                        }

                                        amountSamples++;
                                    }

                                    if(CurrentRun == totalSteps + 1)
                                    {
                                        break;
                                    }
                                    // If the samples are reached then add the sensor to the buffer list of sensors that have their samples reached.
                                    else if (samplesReached)
                                    {
                                        LiveModel.SamplesReachedEvent.Invoke(device.UnitIdentifier);
                                        // Inrease the -internal-  run.
                                        if (CurrentRun != totalSteps + 1)
                                            CurrentRun++;
                                        break;
                                    }
                                }
                                #endregion
                            }

                            if (CurrentRun == totalSteps + 1)
                            {
                                break;
                            }
                        }
                    }
                }));
            }

            // Run the tasks all together.
            parallelTasks.ForEach(x => x.Start());
        }

        List<int> SensorsIDSamplesReached;

        /// <summary>
        /// If the samples are reached then increase the SyncRun. SyncRun is responsible for synchronising the actual currentrun with the 
        /// run that should be going on at the moment. If the samples are reached then the run needs to go to the second run.
        /// </summary>
        /// <param name="sensorId">The sensor identifier.</param>
        private void LiveModel_SamplesReachedEvent(int sensorId)
        {
            if (SensorsIDSamplesReached != null)
            {
                int amountSamplesReached = 0;

                if (!SensorsIDSamplesReached.Contains(sensorId))
                {
                    SensorsIDSamplesReached.Add(sensorId);
                }

                // CHeck for each device if the sensor has reached it's samples.
                SensorsIDSamplesReached.ForEach(x =>
                {
                    foreach (BronkhorstDevice dev in devices)
                    {
                        if (x == dev.UnitIdentifier)
                        {
                            amountSamplesReached++;
                        }
                    }
                });

                // If all the devices (devices.count) reached their samples amount then..
                if (amountSamplesReached == devices.Count)
                {
                    beginFlowSp += (float)stepHeight;

                    elFlow.WriteRegister(BronkhorstRegisters.GetRegister("FSetpoint", BronkhorstDeviceType.EL_FLOW), (float)beginFlowSp);

                    // Increase the synchronised run.
                    // All the sensors will check this variable and increase their 'CurrentRun' until the samples are reached.
                    // after that this will repeat.
                    SyncRun++;

                    // Clear. 
                    SensorsIDSamplesReached.Clear();
                }
            }
        }
    }
}
