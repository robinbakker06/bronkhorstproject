﻿using BronkhorstModbusLibrary.Communication;
using BronkhorstModbusLibrary.Communication.Modbus.Bronkhorst;
using BronkhorstModbusLibrary.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace BronkhorstApp.Model
{
    public delegate void MeasureReceived(object value, double milisecs);
    public delegate void MeasureReceivedParallel(object value, double milisecs, object elFlowValue, int sensorid, double elflowsp);

    public class SettlingModel
    {
        public event MeasureReceived MeasureReceivedEvent;


        public CancellationTokenSource MeasureTokenSource;
        public CancellationToken MeasureToken;

        /// <summary>
        /// Starts the measure process to calculate the settling time.
        /// </summary>
        /// <param name="flowSp">The flow setpoint in the specified capacity unit</param>
        /// <param name="startdelay">The startdelay. (in seconds)</param>
        /// <param name="device">The device to start the measure process</param>
        /// <exception cref="NullReferenceException">Device is null!</exception>
        public async void StartMeasure(double flowSp, int startdelay, BronkhorstDevice device)
        {
            if (device == null)
                throw new NullReferenceException("Device is null!");

            MeasureTokenSource = new CancellationTokenSource();
            MeasureToken = MeasureTokenSource.Token;

            // Make sure to connect to the device 
            device.ModbusConnect();

            //Write InitReset to 64 (means that we can access 'locked' parameters.
            device.WriteRegister(BronkhorstRegisters.GetRegister("Initreset", device.DeviceType), 64);

            // Reset FSetpoint to start clean.
            device.WriteRegister(BronkhorstRegisters.GetRegister("FSetpoint",  device.DeviceType), 0f);

            //Create a task to avoid blocking the interface
            await Task.Factory.StartNew(() =>
            {
                DateTime start = DateTime.Now;

                // Define start parameters (secons to wait, step time)
                bool startDelayReached = false;

                DateTime startTime = DateTime.Now;
                // If there is not cancellation required.. then continue.
                double mSecElapsed;
                while (!MeasureToken.IsCancellationRequested)
                {
                    // Determine milliseconds elapsed.
                    mSecElapsed = (DateTime.Now - startTime).TotalMilliseconds;

                    // if the current running time is higher than the start delay (configured by user) then write the setpoint (configured by user)
                    if (mSecElapsed >= (startdelay * 1000) && !startDelayReached)
                    {
                        device.WriteRegister(BronkhorstRegisters.GetRegister("FSetpoint", device.DeviceType), (float)flowSp);
                        startDelayReached = true;
                    }

                    float val = 0;
                    // Read the measured value
                    val = (float)device.ReadRegister(BronkhorstRegisters.GetRegister("FMeasure", device.DeviceType));

                    // Determine the time it took.
                    DateTime end = DateTime.Now;
                    TimeSpan diff = end - start;

                    // Initiate event invocation to update the UI with the new datapoint (value and the miliseconds)
                    MeasureReceivedEvent?.Invoke(val, diff.TotalMilliseconds);
                    mSecElapsed += (DateTime.Now - startTime).TotalMilliseconds;
                }

                //Reset the setpoint and disconnect
                device.WriteRegister(BronkhorstRegisters.GetRegister("FSetpoint", device.DeviceType), 0f);
                device.ModbusDisconnect();

            }, MeasureToken);
        }
    }
}
