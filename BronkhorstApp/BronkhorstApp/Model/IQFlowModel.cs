﻿using BronkhorstApp.Model.JSON;
using BronkhorstModbusLibrary.Communication;
using BronkhorstModbusLibrary.Communication.Modbus;
using BronkhorstModbusLibrary.Communication.Modbus.Bronkhorst;
using BronkhorstModbusLibrary.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.Model
{
    public class IQFlowModel
    {
        /// <summary>
        /// Adds the device to the BronkhorstSetup Static class and afterwards Saves the device as JSOn on the disk.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="id">The identifier of the device.</param>
        /// <param name="settings">The configuration settings (Modbus)</param>
        /// <returns></returns>
        public bool SaveDevice(string name, int id, ModbusSettings settings)
        {
            if (BronkhhorstSetup.Devices == null || BronkhhorstSetup.Devices.Where(x => x.DeviceName.ToLower() == name.ToLower()).FirstOrDefault() == null)
            {
                BronkhorstDevice device = null;
                // If the device is added to the setup then save as JSON.
                if (BronkhhorstSetup.AddDevice(name, id, BronkhorstModbusLibrary.Enumerations.BronkhorstDeviceType.IQ_FLOW_MODULE, settings, out device))
                {
                    if (device != null)
                    {
                        return JSONModel.SaveObject<BronkhorstDevice>(device);
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return false;
        }
    }
}
