﻿using BronkhorstApp.Model.JSON;
using BronkhorstModbusLibrary.Communication;
using BronkhorstModbusLibrary.Communication.Modbus;
using BronkhorstModbusLibrary.Communication.Modbus.Bronkhorst;
using BronkhorstModbusLibrary.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.Model
{
    public class ELFlowModel
    {
        /// <summary>
        /// Adds the device to the BronkhorstSetup and Saves the device to the disk using JSON. Also tests the connection using wink.
        /// </summary>
        /// <param name="name">The name of the device</param>
        /// <param name="id">The identifier.</param>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        public bool SaveDevice(string name, int id, ModbusSettings settings)
        {
            if (BronkhhorstSetup.Devices == null || BronkhhorstSetup.Devices.Where(x => x.DeviceName.ToLower() == name.ToLower()).FirstOrDefault() == null)
            {
                BronkhorstDevice device = null;
                if(BronkhhorstSetup.AddDevice(name, id, BronkhorstModbusLibrary.Enumerations.BronkhorstDeviceType.EL_FLOW, settings, out device))
                {
                    if(device != null)
                    {
                        device.ModbusConnect();
                        device.WriteRegister(BronkhorstRegisters.GetRegister("Wink", device.DeviceType), 14592);
                        device.ModbusDisconnect();

                        return JSONModel.SaveObject<BronkhorstDevice>(device);
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return false;
        }
    }
}
