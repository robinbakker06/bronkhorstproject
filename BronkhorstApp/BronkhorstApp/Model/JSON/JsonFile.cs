﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;

namespace BronkhorstApp.Model.JSON
{
    /// <summary>
    /// Represents a JSON file of the T type.
    /// </summary>
    /// <typeparam name="T">An JSON file of the given type.</typeparam>
    public class JsonFile<T>
    {
        /// <summary>
        /// Gets or sets the full path.
        /// </summary>
        /// <value>
        /// The full path.
        /// </value>
        public string FullPath { get; set; }

        /// <summary>
        /// Gets or sets the type of the object.
        /// </summary>
        /// <value>
        /// The type of the object.
        /// </value>
        public T ObjectType { get; set; }

        /// <summary>
        /// Gets or sets the file settings. Basic file info of windows.
        /// </summary>
        /// <value>
        /// The file settings.
        /// </value>
        public FileInfo FileSettings { get; set; }

        /// <summary>
        /// Gets or sets the json of the object type.
        /// </summary>
        /// <value>
        /// The json format.
        /// </value>
        public string JSONFormat { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonFile{T}"/> class.
        /// </summary>
        /// <param name="data">The data.</param>
        public JsonFile(T data)
        {
            this.ObjectType = data;
        }

        /// <summary>
        /// Replaces the object by the 
        /// </summary>
        /// <param name="newObj">The new object.</param>
        /// <returns></returns>
        public bool ReplaceObject(T newObj)
        {
            try
            {                                
                this.ObjectType = newObj;

                File.Delete(FullPath);
                File.WriteAllText(FullPath, JsonConvert.SerializeObject(newObj, Formatting.Indented,
                new JsonSerializerSettings
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.None
                }));

                this.FileSettings = new FileInfo(FullPath);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Replaces the parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <param name="replacement">The replacement.</param>
        public void ReplaceParameter(string parameter, object replacement)
        {
            JObject o = JObject.FromObject(ObjectType);

            IEnumerable<JToken> tokenvalues = o.SelectTokens("$.." + parameter);

            foreach (JToken t in tokenvalues)
            {
                t.Replace(JToken.FromObject(replacement));
            }

            File.WriteAllText(FullPath, JsonConvert.SerializeObject(o, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    }));

        }

        /// <summary>
        /// Adds the given object to a list.
        /// </summary>
        /// <param name="list">The list to add the object to</param>
        /// <param name="addition">The object to add to the list</param>
        public void AddToList(string list, object addition)
        {
            JObject o = JObject.FromObject(ObjectType);

            JArray listAsArray = (JArray)o[list];

            JObject additionObject = JObject.Parse(JsonConvert.SerializeObject(addition, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.None
                    }));

            listAsArray.Add(additionObject);

            File.WriteAllText(FullPath, JsonConvert.SerializeObject(o, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.None
                    }));

        }

        /// <summary>
        /// Removes this file.
        /// </summary>
        public void Remove()
        {
            File.Delete(FullPath);
        }

        /// <summary>
        /// Replaces the object.  (NOT IMPLEMENTED)
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <param name="replacement">The replacement.</param>
        /// <param name="propertyWhere">The property where.</param>
        /// <param name="whereValue">The where value.</param>
        public void ReplaceObject(string parameter, string replacement, string propertyWhere, string whereValue)
        {
            //ToDO
        }
    }
}