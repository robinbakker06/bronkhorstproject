﻿using BronkhorstApp.Model.JSON;
using BronkhorstModbusLibrary.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class DeviceModel
    {
        /// <summary>
        /// Retrieves the existings devices from the disk. Specified in JSONModel.
        /// </summary>
        /// <returns></returns>
        public List<BronkhorstDevice> RetrieveExistingsDevices()
        {
            return JSONModel.GetAllObjects<BronkhorstDevice>();
        }
    }
}
