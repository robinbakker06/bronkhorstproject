﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.ViewModel.Mapping
{
    /// <summary>
    /// Data class for the measured values at a certain time. 
    /// This gets plotted into a graph. 
    /// </summary>
    public class ELIQValue
    {
        /// <summary>
        /// Gets or sets the el flow setpoint.
        /// </summary>
        /// <value>
        /// The el flow setpoint.
        /// </value>
        public double ELFlowSetpoint { get; set; }

        /// <summary>
        /// Gets or sets the sensor dbid.
        /// </summary>
        /// <value>
        /// The sensor dbid.
        /// </value>
        public int SensorDBID { get; set; }

        /// <summary>
        /// Gets or sets the el flow value.
        /// </summary>
        /// <value>
        /// The el flow value.
        /// </value>
        public double ELFlowValue { get; set; }

        /// <summary>
        /// Gets or sets the iq flow value.
        /// </summary>
        /// <value>
        /// The iq flow value.
        /// </value>
        public double IQFlowValue { get; set; }


        /// <summary>
        /// Gets or sets the mili sec from start.
        /// </summary>
        /// <value>
        /// The mili sec from start.
        /// </value>
        public double MSecFromStart { get; set; }
    }
}
