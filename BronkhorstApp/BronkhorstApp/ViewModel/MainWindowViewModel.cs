﻿using BronkhorstApp.Helpers;
using BronkhorstApp.Model;
using BronkhorstApp.View.Popups;
using BronkhorstApp.View.Popups.Informative;
using BronkhorstApp.ViewModel.Popups;
using BronkhorstApp.ViewModel.Popups.Informative;
using BronkhorstModbusLibrary.Devices;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BronkhorstApp.ViewModel
{

    /// <summary>
    /// Viewmodel for the mainwindow.
    /// </summary>
    /// <seealso cref="BronkhorstApp.Helpers.ObservableObject" />
    public class MainWindowViewModel : ObservableObject
    {
        private MainModel Model { get; set; }

        public RelayCommand StartCommand => new RelayCommand(x => StartMeasure());
        public RelayCommand DeviceSetting => new RelayCommand(x => DeviceSett());

        public RelayCommand CalculateSettlingCommand => new RelayCommand(x => CalculateSettlingCommand_Execute());

        public RelayCommand AutoDeviceCommand => new RelayCommand(x => AutoDeviceCommand_Execute());

        #region DataBindingss
        private string flowsetpoint;
        private string measurepoints;
        private string measuretime;
        private string sensor1;
        private string sensor2;
        private string sensor3;
        private string sensor4;
        private string sensor5;
        private string sensor6;
        private string samplerate;
        private string settlingtime;
        private string endflowsetpoint;

        /// <summary>
        /// Gets or sets thesettling time
        /// </summary>
        /// <value>
        /// The settling time in seconds
        /// </value>
        public string SettlingTime
        {
            get { return settlingtime; }
            set
            {
                settlingtime = value;
                OnPropertyChanged(() => SettlingTime);
            }
        }

        /// <summary>
        /// Gets or sets the flow set point.
        /// </summary>
        /// <value>
        /// The flow set point.
        /// </value>
        public string FlowSetPoint
        {
            get { return flowsetpoint; }
            set
            {
                flowsetpoint = value;
                OnPropertyChanged(() => FlowSetPoint);
            }
        }

        /// <summary>
        /// Gets or sets the end (max) flow set point.
        /// </summary>
        /// <value>
        /// The end (max) flow set point.
        /// </value>
        public string EndFlowSetPoint
        {
            get { return endflowsetpoint; }
            set
            {
                endflowsetpoint = value;
                OnPropertyChanged(() => EndFlowSetPoint);
            }
        }

        /// <summary>
        /// Gets or sets the measure points.
        /// </summary>
        /// <value>
        /// The measure points.
        /// </value>
        public string MeasurePoints
        {
            get { return measurepoints; }
            set
            {
                measurepoints = value;
                OnPropertyChanged(() => MeasurePoints);
            }
        }

        /// <summary>
        /// Gets or sets the sample rate
        /// </summary>
        /// <value>
        /// The sample rate value
        /// </value>
        public string SampleRate
        {
            get { return samplerate; }
            set
            {
                samplerate = value;
                OnPropertyChanged(() => SampleRate);
            }
        }
        /// <summary>
        /// Gets or sets the measure time.
        /// </summary>
        /// <value>
        /// The measure time.
        /// </value>
        public string MeasureTime
        {
            get { return measuretime; }
            set
            {
                measuretime = value;
                OnPropertyChanged(() => MeasureTime);
            }
        }

        /// <summary>
        /// Gets or sets the sensor1.
        /// </summary>
        /// <value>
        /// The sensor1.
        /// </value>
        public string Sensor1
        {
            get { return sensor1; }
            set
            {
                sensor1 = value;
                OnPropertyChanged(() => Sensor1);
            }
        }

        /// <summary>
        /// Gets or sets the sensor2.
        /// </summary>
        /// <value>
        /// The sensor2.
        /// </value>
        public string Sensor2
        {
            get { return sensor2; }
            set
            {
                sensor2 = value;
                OnPropertyChanged(() => Sensor2);
            }
        }
        public string Sensor3
        {
            get { return sensor3; }
            set
            {
                sensor3 = value;
                OnPropertyChanged(() => Sensor3);
            }
        }

        /// <summary>
        /// Gets or sets the sensor4.
        /// </summary>
        /// <value>
        /// The sensor4.
        /// </value>
        public string Sensor4
        {
            get { return sensor4; }
            set
            {
                sensor4 = value;
                OnPropertyChanged(() => Sensor4);
            }
        }

        /// <summary>
        /// Gets or sets the sensor5.
        /// </summary>
        /// <value>
        /// The sensor5.
        /// </value>
        public string Sensor5
        {
            get { return sensor5; }
            set
            {
                sensor5 = value;
                OnPropertyChanged(() => Sensor5);
            }
        }
        /// <summary>
        /// Gets or sets the sensor6.
        /// </summary>
        /// <value>
        /// The sensor6.
        /// </value>
        public string Sensor6
        {
            get { return sensor6; }
            set
            {
                sensor6 = value;
                OnPropertyChanged(() => Sensor6);
            }
        }
        #endregion

        private BronkhorstDevice sensor1device;
        private BronkhorstDevice sensor2device;
        private BronkhorstDevice sensor3device;
        private BronkhorstDevice sensor4device;
        private BronkhorstDevice sensor5device;
        private BronkhorstDevice sensor6device;

        public BronkhorstDevice Sensor1Device
        {
            get
            {
                return sensor1device;
            }
            set
            {
                sensor1device = value;
                OnPropertyChanged(() => Sensor1Device);
            }
        }

        public BronkhorstDevice Sensor2Device
        {
            get
            {
                return sensor2device;
            }
            set
            {
                sensor2device = value;
                OnPropertyChanged(() => Sensor2Device);
            }
        }

        public BronkhorstDevice Sensor3Device
        {                           
            get
            {
                return sensor3device;
            }
            set
            {
                sensor3device = value;
                OnPropertyChanged(() => Sensor3Device);
            }
        }

        public BronkhorstDevice Sensor4Device
        {
            get
            {
                return sensor4device;
            }
            set
            {
                sensor4device = value;
                OnPropertyChanged(() => Sensor4Device);
            }
        }

        public BronkhorstDevice Sensor5Device
        {
            get
            {
                return sensor5device;
            }
            set
            {
                sensor5device = value;
                OnPropertyChanged(() => Sensor5Device);
            }
        }

        public BronkhorstDevice Sensor6Device
        {
            get
            {
                return sensor6device;
            }
            set
            {
                sensor6device = value;
                OnPropertyChanged(() => Sensor6Device);
            }
        }

        private ObservableCollection<BronkhorstDevice> deviceList;
        public ObservableCollection<BronkhorstDevice> DeviceList
        {
            get { return deviceList; }
            set
            {
                deviceList = value;
                OnPropertyChanged(() => DeviceList);
            }
        }

        public MainWindowViewModel()
        {
            this.Model = new MainModel();
            DeviceList = new ObservableCollection<BronkhorstDevice>(Model.RetrieveAllIQFlowModules());

            FlowSetPoint = "0";
            EndFlowSetPoint = "2000";
            MeasurePoints = "10";
            SampleRate = "5";
            SettlingTime = "1";
            MeasureTime = "5";

            AutoDeviceCommand_Execute();
        }

        /// <summary>
        /// Starts the measuring process in the LiveView dialog.
        /// </summary>
        public async void StartMeasure()
        {
            Sensor1Device.DBID = Convert.ToInt16(Sensor1);
            Sensor2Device.DBID = Convert.ToInt16(Sensor2);
            Sensor3Device.DBID = Convert.ToInt16(Sensor3);
            Sensor4Device.DBID = Convert.ToInt16(Sensor4);
            Sensor5Device.DBID = Convert.ToInt16(Sensor5);
            Sensor6Device.DBID = Convert.ToInt16(Sensor6);

            List<BronkhorstDevice> devList = new List<BronkhorstDevice>();
            devList.Add(Sensor1Device);
            devList.Add(Sensor2Device);
            devList.Add(Sensor3Device);
            devList.Add(Sensor4Device);
            devList.Add(Sensor5Device);
            devList.Add(Sensor6Device);

            LiveView liveView = new LiveView() { DataContext = new LiveViewViewModel(FlowSetPoint, EndFlowSetPoint, MeasureTime, SampleRate,
                MeasurePoints, SettlingTime, devList) };
            await DialogHost.Show(liveView);
        }

        /// <summary>
        /// Opens the configuration dialog
        /// </summary>
        public async void DeviceSett()
        {
            DeviceSettingView deviceSetting = new DeviceSettingView();
            deviceSetting.DataContext = new DeviceSettingViewModel();
            await DialogHost.Show(deviceSetting);

            DeviceList = new ObservableCollection<BronkhorstDevice>(Model.RetrieveAllIQFlowModules());
        }

        /// <summary>
        ///Command to calculate the settling time. Opens the settling dialog
        /// </summary>
        private async void CalculateSettlingCommand_Execute()
        {
            SettlingView settlingView = new SettlingView();
            await DialogHost.Show(settlingView);
        }

        private void AutoDeviceCommand_Execute()
        {
            List<BronkhorstDevice> deviceList = Model.RetrieveAllIQFlowModules();

            List<BronkhorstDevice> deviceListOrdered = deviceList.OrderBy(x => x.UnitIdentifier).ToList();

            Sensor1Device = deviceListOrdered[0];
            Sensor2Device = deviceListOrdered[1];
            Sensor3Device = deviceListOrdered[2];
            Sensor4Device = deviceListOrdered[3];
            Sensor5Device = deviceListOrdered[4];
            Sensor6Device = deviceListOrdered[5];

            MessageBox.Show("FIlled in the devices.");
        }

    }
}
