﻿using BronkhorstApp.Helpers;
using BronkhorstApp.Model;
using BronkhorstApp.Model.JSON;
using BronkhorstModbusLibrary.Devices;
using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.ViewModel.Popups
{
    public class SettlingViewModel : ObservableObject
    {
        #region CHART
        public SeriesCollection SeriesCollection { get; set; }

        public ChartValues<double> SetpointValues
        {
            get; set;
        }

        private string[] timelabels;
        public string[] TimeLabels
        {
            get
            {
                return timelabels;
            }
            set
            {
                timelabels = value;
                OnPropertyChanged(() => TimeLabels);
            }
        }

        private Func<double, string> setpointformatter;
        public Func<double, string> SetpointFormatter
        {
            get { return setpointformatter; }
            set
            {
                setpointformatter = value;
                OnPropertyChanged(() => SetpointFormatter);
            }
        }

        public string[] Labels { get; set; }

        public Func<double, string> YFormatter { get; set; }
        public Func<double, string> XFormatter { get; set; }

        #endregion
        private SettlingModel Model
        {
            get; set;
        }

        #region RelayCommands
        public RelayCommand StartCommand => new RelayCommand(x => StartCommand_Execute());
        public RelayCommand StopCommand => new RelayCommand(x => StopCommand_Execute());
        #endregion

        #region Data Binding
        private BronkhorstDevice selecteddevice;
        public BronkhorstDevice SelectedDevice
        {
            get { return selecteddevice; }
            set
            {
                selecteddevice = value;
                OnPropertyChanged(() => SelectedDevice);
            }
        }

        private ObservableCollection<BronkhorstDevice> availabledevices;
        public ObservableCollection<BronkhorstDevice> AvailableDevices
        {
            get { return availabledevices; }
            set
            {
                availabledevices = value;
                OnPropertyChanged(() => AvailableDevices);
            }
        }


        private string flowsetpoint;
        public string FlowSetpoint
        {
            get { return flowsetpoint; }
            set
            {
                flowsetpoint = value;
                OnPropertyChanged(() => FlowSetpoint);
            }
        }

        private string startdelay;
        public string StartDelay
        {
            get { return startdelay; }
            set
            {
                startdelay = value;
                OnPropertyChanged(() => StartDelay);
            }
        }
        #endregion

        public SettlingViewModel()
        {
            Model = new SettlingModel();

            SetpointValues = new ChartValues<double>();
            TimeLabels = new string[1];
            TimeLabels[0] = "0";

            AvailableDevices = new ObservableCollection<BronkhorstDevice>(JSONModel.GetAllObjects<BronkhorstDevice>());

            SeriesCollection = new SeriesCollection
                {
                    new LineSeries
                    {
                        Title = "Flow ml/min",
                        Values = SetpointValues,
                        ScalesYAt = 0,
                        PointGeometry = null
                    }
                };

            SetpointFormatter = v => (v.ToString("0.00") + " ml/min");
            XFormatter = v => (v.ToString("0.00") + " s");

        }

        private void StartCommand_Execute()
        {
            Model.MeasureReceivedEvent += MeasurePointCallBack;

            Model.StartMeasure(double.Parse(FlowSetpoint), Convert.ToInt16(startdelay), SelectedDevice);
        }

        private void StopCommand_Execute()
        {
            Model.MeasureTokenSource.Cancel();
        }


        /// <summary>
        /// Call back for the measure points.. Adds the measured value and time to the graph.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="totalms">The total MS since start</param>
        public void MeasurePointCallBack(object value, double totalms)
        {
            Type typeVal = value.GetType();

            if (typeVal == typeof(float))
            {
                decimal dec = new decimal(((float)value));
                List<string> tempLabels = TimeLabels.ToList();
                tempLabels.Add((totalms / 1000).ToString("0.00"));

                TimeLabels = tempLabels.ToArray();
                SetpointValues.Add((double)dec);
            }
        }
    }
}
