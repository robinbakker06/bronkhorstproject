﻿using BronkhorstApp.Helpers;
using BronkhorstModbusLibrary.Enumerations;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.ViewModel.Popups.ConfigurationScreens
{
    public class AddDeviceViewModel : ObservableObject
    {
        private BronkhorstDeviceType selecteddevicetype;

        /// <summary>
        /// Gets or sets the type of the selected device.
        /// </summary>
        /// <value>
        /// The type of the selected device.
        /// </value>
        public BronkhorstDeviceType SelectedDeviceType
        {
            get
            {
                return selecteddevicetype;
            }
            set
            {
                if(value != selecteddevicetype)
                {
                    switch(value)
                    {
                        case BronkhorstDeviceType.EL_FLOW:
                            DeviceContentControl = new ELFlowViewModel();
                            break;
                        case BronkhorstDeviceType.IQ_FLOW_MODULE:
                            DeviceContentControl = new IQFlowViewModel();
                            break;
                    }
                }

                selecteddevicetype = value;
                OnPropertyChanged(() => SelectedDeviceType);
            }
        }

        private object devicecontentcontrol;
        /// <summary>
        /// Gets or sets the device content control.
        /// </summary>
        /// <value>
        /// The device content control.
        /// </value>
        public object DeviceContentControl
        {
            get { return devicecontentcontrol; }
            set
            {
                devicecontentcontrol = value;
                OnPropertyChanged(() => DeviceContentControl);
            }
        }

        private ObservableCollection<BronkhorstDeviceType> devicetypelist;
        /// <summary>
        /// Gets or sets the device type list.
        /// </summary>
        /// <value>
        /// The device type list.
        /// </value>
        public ObservableCollection<BronkhorstDeviceType> DeviceTypeList
        {
            get { return devicetypelist; }
            set
            {
                devicetypelist = value;
                OnPropertyChanged(() => DeviceTypeList);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddDeviceViewModel"/> class.
        /// </summary>
        public AddDeviceViewModel()
        {
            DeviceTypeList = new ObservableCollection<BronkhorstDeviceType>(Enum.GetValues(typeof(BronkhorstDeviceType)).Cast<BronkhorstDeviceType>());
        }



    }
}
