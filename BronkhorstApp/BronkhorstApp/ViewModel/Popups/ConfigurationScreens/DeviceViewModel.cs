﻿using BronkhorstApp.Helpers;
using BronkhorstApp.Model;
using BronkhorstApp.Model.JSON;
using BronkhorstApp.View.Popups.ConfigurationScreens;
using BronkhorstModbusLibrary.Devices;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.ViewModel.Popups.ConfigurationScreens
{
    public class DeviceViewModel : ObservableObject
    {
        private DeviceModel Model { get; set; }

        private ObservableCollection<BronkhorstDevice> devices;
        private BronkhorstDevice selecteddevice;

        /// <summary>
        /// Gets or sets the selected device from the combobox.
        /// </summary>
        /// <value>
        /// The selected device.
        /// </value>
        public BronkhorstDevice SelectedDevice
        {
            get { return selecteddevice; }
            set
            {
                selecteddevice = value;
                OnPropertyChanged(() => SelectedDevice);
            }
        }

        /// <summary>
        /// Gets or sets the devices.
        /// </summary>
        /// <value>
        /// The devices.
        /// </value>
        public ObservableCollection<BronkhorstDevice> Devices
        {
            get { return devices; }
            set
            {
                devices = value;
                OnPropertyChanged(() => Devices);
            }

        }

        /// <summary>
        /// Gets the add device command.
        /// </summary>
        /// <value>
        /// The add device command.
        /// </value>
        public RelayCommand AddDeviceCommand => new RelayCommand(x => AddDeviceCommand_Execute());

        /// <summary>
        /// Gets the edit device command.
        /// </summary>
        /// <value>
        /// The edit device command.
        /// </value>
        public RelayCommand EditDeviceCommand => new RelayCommand(x => EditDeviceCommand_Execute());

        /// <summary>
        /// Gets the remove device command.
        /// </summary>
        /// <value>
        /// The remove device command.
        /// </value>
        public RelayCommand RemoveDeviceCommand => new RelayCommand(x => RemoveDeviceCommand_Execute());

        public DeviceViewModel()
        {
            Model = new DeviceModel();
            Devices = new ObservableCollection<BronkhorstDevice>(Model.RetrieveExistingsDevices());
        }

        /// <summary>
        /// Command to add the device. Opens the dialog to add a new device.
        /// </summary>
        private async void AddDeviceCommand_Execute()
        {

            AddDeviceView view = new AddDeviceView()
            {
                DataContext = new AddDeviceViewModel()
            };

            await DialogHost.Show(view, "ConfigurationPopupHost");

            Devices = new ObservableCollection<BronkhorstDevice>(Model.RetrieveExistingsDevices());
        }

        /// <summary>
        /// Edits the device command execute.  (not implemented)
        /// </summary>
        private async void EditDeviceCommand_Execute()
        {
            // Not implemented.
        }

        /// <summary>
        /// Relay command to remove the selected device. Removes the device from the disk.
        /// </summary>
        private void RemoveDeviceCommand_Execute()
        {
            JSONModel.RemoveObject<BronkhorstDevice>(SelectedDevice);
            Devices = new ObservableCollection<BronkhorstDevice>(Model.RetrieveExistingsDevices());
        }
    }
}
