﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BronkhorstApp.Helpers;
using BronkhorstApp.Model;
using BronkhorstApp.View.Popups;
using MaterialDesignThemes.Wpf;
using BronkhorstModbusLibrary.Communication.Modbus;
using BronkhorstApp.View.Popups.ConfigurationScreens;
using BronkhorstApp.Model.JSON;

namespace BronkhorstApp.ViewModel.Popups.ConfigurationScreens
{
    public class ModbusViewModel : ObservableObject
    {
        private ModbusModel Model { get; set; }

        private ObservableCollection<ModbusSettings> configurations;
        private ModbusSettings selectedconfig;

        /// <summary>
        /// Gets or sets the selected configuration.
        /// </summary>
        /// <value>
        /// The selected configuration.
        /// </value>
        public ModbusSettings SelectedConfig
        {
            get { return selectedconfig; }
            set
            {
                selectedconfig = value;
                OnPropertyChanged(() => SelectedConfig);
            }
        }

        /// <summary>
        /// Gets or sets the configurations.
        /// </summary>
        /// <value>
        /// The configurations.
        /// </value>
        public ObservableCollection<ModbusSettings> Configurations
        {
            get { return configurations; }
            set
            {
                configurations = value;
                OnPropertyChanged(() => Configurations);
            }

        }

        /// <summary>
        /// Gets the add configuration command.
        /// </summary>
        /// <value>
        /// The add configuration command.
        /// </value>
        public RelayCommand AddConfigurationCommand => new RelayCommand(x => AddConfigurationCommand_Execute());

        /// <summary>
        /// Gets the edit configuration command.
        /// </summary>
        /// <value>
        /// The edit configuration command.
        /// </value>
        public RelayCommand EditConfigurationCommand => new RelayCommand(x => EditConfigurationCommand_Execute());

        /// <summary>
        /// Gets the remove configuration command.
        /// </summary>
        /// <value>
        /// The remove configuration command.
        /// </value>
        public RelayCommand RemoveConfigurationCommand => new RelayCommand(x => RemoveConfigurationCommand_Execute());

        /// <summary>
        /// Initializes a new instance of the <see cref="ModbusViewModel"/> class.
        /// </summary>
        public ModbusViewModel()
        {
            Model = new ModbusModel();
            Configurations = new ObservableCollection<ModbusSettings>(Model.RetrieveExistingsConfigurations());
        }

        /// <summary>
        /// command to add a new configuration. Opens a dialog to add new modbus configuration
        /// </summary>
        private async void AddConfigurationCommand_Execute()
        {
            ModbusConfigurationView view = new ModbusConfigurationView()
            {
                DataContext = new ModbusConfigurationViewModel(false)
            };

            await DialogHost.Show(view, "ConfigurationPopupHost");
        }

        /// <summary>
        /// Command to edit a configuration. Opens a edit dialog for the selected configuration
        /// </summary>
        private async void EditConfigurationCommand_Execute()
        {
            ModbusConfigurationView view = new ModbusConfigurationView()
            {
                DataContext = new ModbusConfigurationViewModel(true, SelectedConfig)
            };

            await DialogHost.Show(view, "ConfigurationPopupHost");
            Configurations = new ObservableCollection<ModbusSettings>(Model.RetrieveExistingsConfigurations());
        }

        /// <summary>
        /// Command to Remove the selected configuration
        /// </summary>
        private void RemoveConfigurationCommand_Execute()
        {
            JSONModel.RemoveObject<ModbusSettings>(SelectedConfig);
            Configurations = new ObservableCollection<ModbusSettings>(Model.RetrieveExistingsConfigurations());
        }
    }
}
