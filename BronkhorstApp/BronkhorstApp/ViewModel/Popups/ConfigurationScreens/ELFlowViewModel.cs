﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BronkhorstApp.Helpers;
using BronkhorstApp.Model;
using BronkhorstApp.View.Popups;
using MaterialDesignThemes.Wpf;
using BronkhorstModbusLibrary.Communication.Modbus;
using BronkhorstApp.Model.JSON;

namespace BronkhorstApp.ViewModel.Popups.ConfigurationScreens
{
    public class ELFlowViewModel : ObservableObject
    {
        private ELFlowModel Model { get; set; }
        public RelayCommand SaveELFlow => new RelayCommand(x => SaveCommand_Execute());

        string name;
        int elflowLSD;
        int elflowMSD;
        /// <summary>
        /// Gets or sets the name  of the EL FLOW (symbolic name)
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                return name;

            }
            set
            {
                name = value;
                OnPropertyChanged(() => Name);
            }
        }

        /// <summary>
        /// Gets or sets the el flow LSD.   (least significant digit)
        /// </summary>
        /// <value>
        /// The el flow LSD.
        /// </value>
        public int ELFlowLSD
        {
            get { return elflowLSD; }
            set
            {
                elflowLSD = value;
                OnPropertyChanged(() => ELFlowLSD);
            }
        }

        /// <summary>
        /// Gets or sets the el flow MSD.  (most significant digit)
        /// </summary>
        /// <value>
        /// The el flow MSD.
        /// </value>
        public int ELFlowMSD
        {
            get { return elflowMSD; }
            set
            {
                elflowMSD = value;
                OnPropertyChanged(() => ELFlowMSD);
            }
        }

        private ModbusSettings selectedconfig;

        /// <summary>
        /// Gets or sets the selected configuration.
        /// </summary>
        /// <value>
        /// The selected configuration.
        /// </value>
        public ModbusSettings SelectedConfig
        {
            get { return selectedconfig; }
            set
            {
                selectedconfig = value;
                OnPropertyChanged(() => SelectedConfig);
            }
        }

        private ObservableCollection<ModbusSettings> availableconfigs;
        /// <summary>
        /// Gets or sets the available configurations.
        /// </summary>
        /// <value>
        /// The available configurations.
        /// </value>
        public ObservableCollection<ModbusSettings> AvailableConfigurations
        {
            get { return availableconfigs; }
            set
            {
                availableconfigs = value;
                OnPropertyChanged(() => AvailableConfigurations);
            }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ELFlowViewModel"/> class.
        /// </summary>
        public ELFlowViewModel()
        {
            Model = new ELFlowModel();
            AvailableConfigurations = new ObservableCollection<ModbusSettings>(JSONModel.GetAllObjects<ModbusSettings>());
        }

        /// <summary>
        /// Command to save the EL-FLOW 
        /// </summary>
        private void SaveCommand_Execute()
        {
            if (Name != "" && SelectedConfig != null)
                Model.SaveDevice(Name, Convert.ToInt32(elflowLSD), SelectedConfig);
        
        }
    }
}
