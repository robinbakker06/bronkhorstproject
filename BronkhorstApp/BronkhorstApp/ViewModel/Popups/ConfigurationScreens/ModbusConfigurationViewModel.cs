﻿using BronkhorstApp.Helpers;
using BronkhorstApp.Model;
using BronkhorstModbusLibrary.Communication.Modbus;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.ViewModel.Popups.ConfigurationScreens
{
    /// <summary>
    /// THe modbus configuration viewmodel. part of the MVVM framework.
    /// </summary>
    /// <seealso cref="BronkhorstApp.Helpers.ObservableObject" />
    public class ModbusConfigurationViewModel : ObservableObject
    {
        private ModbusConfigurationModel Model { get; set; }

        public RelayCommand SaveCommand => new RelayCommand(x => SaveCommand_Execute());
        public RelayCommand CloseWindowCommand => new RelayCommand(x => CloseWindowCommand_Execute());

        private string parity;
        private string name;
        private int modbusid;
        private int baudrate;
        private string device;
        private string comport;
        private ObservableCollection<int> baudratevalues;
        private ObservableCollection<string> parityvalues;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged(() => Name);
            }
        }

        /// <summary>
        /// Gets or sets the device.
        /// </summary>
        /// <value>
        /// The device.
        /// </value>
        public string Device
        {
            get { return device; }
            set
            {
                device = value;
                OnPropertyChanged(() => Device);
            }
        }

        /// <summary>
        /// Gets or sets the baudrate.
        /// </summary>
        /// <value>
        /// The baudrate.
        /// </value>
        public int Baudrate
        {
            get { return baudrate; }
            set
            {
                baudrate = value;
                OnPropertyChanged(() => Baudrate);
            }
        }

        /// <summary>
        /// Gets or sets the comport.
        /// </summary>
        /// <value>
        /// The comport.
        /// </value>
        public string Comport
        {
            get { return comport; }
            set
            {
                comport = value;
                OnPropertyChanged(() => Comport);
            }
        }

        /// <summary>
        /// Gets or sets the parity.
        /// </summary>
        /// <value>
        /// The parity.
        /// </value>
        public string Parity
        {
            get { return parity; }
            set
            {
                parity = value;
                OnPropertyChanged(() => Parity);
            }
        }

        /// <summary>
        /// Gets or sets the modbus identifier.
        /// </summary>
        /// <value>
        /// The modbus identifier.
        /// </value>
        public int ModbusID
        {
            get { return modbusid; }
            set
            {
                modbusid = value;
                OnPropertyChanged(() => ModbusID);
            }
        }

        public ObservableCollection<string> PortNames { get; } = new ObservableCollection<string>(SerialPort.GetPortNames());

        /// <summary>
        /// Gets or sets the parityvalues.
        /// </summary>
        /// <value>
        /// The parityvalues.
        /// </value>
        public ObservableCollection<string> Parityvalues
        {
            get { return parityvalues; }
            set
            {
                parityvalues = value;
                OnPropertyChanged(() => Parityvalues);
            }
        }

        public ObservableCollection<int> BaudrateValues
        {
            get { return baudratevalues; }
            set
            {
                baudratevalues = value;
                OnPropertyChanged(() => BaudrateValues);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is editable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is edit; otherwise, <c>false</c>.
        /// </value>
        private bool IsEdit { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModbusConfigurationViewModel"/> class.
        /// </summary>
        /// <param name="isEdit">if set to <c>true</c> [is edit].</param>
        /// <param name="settings">The settings.</param>
        public ModbusConfigurationViewModel(bool isEdit, ModbusSettings settings = null)
        {
            Model = new ModbusConfigurationModel();
            this.IsEdit = isEdit;

            baudratevalues = new ObservableCollection<int>();
            parityvalues = new ObservableCollection<string>();
            baudratevalues.Add(9200);
            baudratevalues.Add(19200);
            baudratevalues.Add(38400);

            parityvalues.Add("even");
            parityvalues.Add("uneven");

            if(settings != null && isEdit)
            {
                Name = settings.Name;
                Baudrate = settings.BaudRate;
                Comport = "COM" + settings.Port;
                Parity = (settings.Parity == System.IO.Ports.Parity.Even)? "even" : "uneven";
            }
        }

        private void SaveCommand_Execute()
        {
            if (!IsEdit)
                Model.SaveSettings(Name, Convert.ToInt32(Comport.Replace("COM", "")), Convert.ToInt32(Baudrate), Parity, ModbusID);
            else
                Model.EditSettings(Name, Convert.ToInt32(Comport.Replace("COM", "")), Convert.ToInt32(Baudrate), Parity, ModbusID);
        }


        /// <summary>
        /// Closes the window 
        /// </summary>
        private void CloseWindowCommand_Execute()
        {
            DialogHost.CloseDialogCommand.Execute(null, null);
        }
    }
}
