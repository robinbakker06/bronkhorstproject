﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BronkhorstApp.Model;
using System.Threading.Tasks;
using BronkhorstApp.Helpers;
using MaterialDesignThemes.Wpf;
using System.Collections.ObjectModel;
using BronkhorstModbusLibrary.Communication.Modbus;
using BronkhorstApp.Model.JSON;

namespace BronkhorstApp.ViewModel.Popups.ConfigurationScreens
{
    /// <summary>
    /// Viewmodel of the IQFlow view. Part of the MVVM framework
    /// </summary>
    /// <seealso cref="BronkhorstApp.Helpers.ObservableObject" />
    public class IQFlowViewModel : ObservableObject
    {
        public RelayCommand SaveIQFLOW => new RelayCommand(x => SaveCommand_Execute());

        private IQFlowModel Model { get; set; }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged(() => Name);
            }
        }

        private ModbusSettings selectedconfig;

        /// <summary>
        /// Gets or sets the selected configuration.
        /// </summary>
        /// <value>
        /// The selected configuration.
        /// </value>
        public ModbusSettings SelectedConfig
        {
            get { return selectedconfig; }
            set
            {
                selectedconfig = value;
                OnPropertyChanged(() => SelectedConfig);
            }
        }

        private ObservableCollection<ModbusSettings> availableconfigs;
        /// <summary>
        /// Gets or sets the available configurations.
        /// </summary>
        /// <value>
        /// The available configurations.
        /// </value>
        public ObservableCollection<ModbusSettings> AvailableConfigurations
        {
            get { return availableconfigs; }
            set
            {
                availableconfigs = value;
                OnPropertyChanged(() => AvailableConfigurations);
            }
        }

        private int iqflowmsd;
        /// <summary>
        /// Gets or sets the el flow MSD.  (most significant digit)
        /// </summary>
        /// <value>
        /// The el flow MSD.
        /// </value>
        public int IQFlowMSD
        {
            get { return iqflowmsd; }
            set
            {
                iqflowmsd = value;
                OnPropertyChanged(() => IQFlowMSD);
            }
        }

        private int iqflowlsd;
        /// <summary>
        /// Gets or sets the el flow MSD.  (most significant digit)
        /// </summary>
        /// <value>
        /// The el flow MSD.
        /// </value>
        public int IQFlowLSD
        {
            get { return iqflowlsd; }
            set
            {
                iqflowlsd = value;
                OnPropertyChanged(() => IQFlowLSD);
            }
        }

        private int iqflowmsd1;
        /// <summary>
        /// Gets or sets the el flow MSD.  (most significant digit)
        /// </summary>
        /// <value>
        /// The el flow MSD.
        /// </value>
        public int IQFlowMSD1
        {
            get { return iqflowmsd1; }
            set
            {
                iqflowmsd1 = value;
                OnPropertyChanged(() => IQFlowMSD1);
            }
        }

        private int iqflowlsd1;
        /// <summary>
        /// Gets or sets the el flow MSD.  (most significant digit)
        /// </summary>
        /// <value>
        /// The el flow MSD.
        /// </value>
        public int IQFlowLSD1
        {
            get { return iqflowlsd1; }
            set
            {
                iqflowlsd1 = value;
                OnPropertyChanged(() => IQFlowLSD1);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IQFlowViewModel"/> class.
        /// </summary>
        public IQFlowViewModel()
        {
            this.Model = new IQFlowModel();
            AvailableConfigurations = new ObservableCollection<ModbusSettings>(JSONModel.GetAllObjects<ModbusSettings>());
        }


        private void SaveCommand_Execute()
        {

            for (int i = IQFlowLSD; i < IQFlowLSD + 3; i++)
            {
                Model.SaveDevice(Name + "_MID" + i, i, SelectedConfig);
            }

            for (int i = IQFlowLSD1; i < IQFlowLSD1 + 3; i++)
            {
                Model.SaveDevice(Name + "_MID" + i, i, SelectedConfig);
            }

            DialogHost.CloseDialogCommand.Execute(null, null);
        }
    }
}
