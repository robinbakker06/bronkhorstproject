﻿using System;
using BronkhorstApp.Helpers;
using MaterialDesignThemes.Wpf;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BronkhorstApp.Model;
using System.Collections.ObjectModel;
using System.IO.Ports;
using BronkhorstApp.ViewModel.Popups.ConfigurationScreens;

namespace BronkhorstApp.ViewModel.Popups
{
    /// <summary>
    /// Describes a configuration item in a listbox.
    /// </summary>
    public class ConfigurationListBoxItem
    {
        /// <summary>
        /// Gets or sets the config name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the screen to redirect to.
        /// </summary>
        /// <value>
        /// The screen.
        /// </value>
        public object Screen { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationListBoxItem"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="screen">The screen.</param>
        public ConfigurationListBoxItem(string name, object screen)
        {
            this.Name = name;
            this.Screen = screen;
        }

        public override string ToString()
        {
            return Name;
        }


    }

    /// <summary>
    /// The Device Setting ViewModel. all view logics are handled here.
    /// </summary>
    /// <seealso cref="BronkhorstApp.Helpers.ObservableObject" />
    public class DeviceSettingViewModel : ObservableObject
    {
        private DeviceSettingModel Model { get; set; }

        private ObservableCollection<ConfigurationListBoxItem> configurationListBoxItems;
        private ConfigurationListBoxItem selectecconfigurationlistboxitem;
        private object configurationscreen;


        /// <summary>
        /// Gets or sets the configuration screen.
        /// </summary>
        /// <value>
        /// The configuration screen.
        /// </value>
        public object ConfigurationScreen
        {
            get { return configurationscreen; }
            set
            {
                configurationscreen = value;
                OnPropertyChanged(() => ConfigurationScreen);
            }
        }


        /// <summary>
        /// Gets or sets the selected configuration listbox item.
        /// </summary>
        /// <value>
        /// The selected configuration listbox item.
        /// </value>
        public ConfigurationListBoxItem SelectecConfigurationListboxItem
        {
            get { return selectecconfigurationlistboxitem; }
            set
            {
                selectecconfigurationlistboxitem = value;
                if (value != null)
                {
                    ConfigurationScreen = value.Screen;
                }
                OnPropertyChanged(() => SelectecConfigurationListboxItem);
            }
        }

        /// <summary>
        /// Gets or sets the ListBox items source.       for the configuration items in the listbox.
        /// </summary>
        /// <value>
        /// The ListBox items source.
        /// </value>
        public ObservableCollection<ConfigurationListBoxItem> ListBoxItemsSource
        {
            get { return configurationListBoxItems; }
            set
            {
                configurationListBoxItems = value;
                OnPropertyChanged(() => ListBoxItemsSource);
            }
        }



        public DeviceSettingViewModel()
        {
            Model = new DeviceSettingModel();
            ListBoxItemsSource = new ObservableCollection<ConfigurationListBoxItem>();

            ListBoxItemsSource.Add(new ConfigurationListBoxItem("ModbusConfiguration", new ModbusViewModel()));
            ListBoxItemsSource.Add(new ConfigurationListBoxItem("Device View", new DeviceViewModel()));

        }

    }

        


    
}
