﻿using BronkhorstApp.Helpers;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.ViewModel.Popups.Informative
{
    /// <summary>
    /// delegate to determine wether the action is confirmed or not. (Not used)
    /// </summary>
    /// <param name="isConfirmed">if set to <c>true</c> [is confirmed].</param>
    public delegate void ActionConfirmResult(bool isConfirmed);

    /// <summary>
    /// Standard viewmodel for action confirmation
    /// </summary>
    /// <seealso cref="BronkhorstApp.Helpers.ObservableObject" />
    public class ConfirmationDialogViewModel : ObservableObject
    {
        public event ActionConfirmResult ActionConfirmEvent;

        List<object> paramList { get; set; }
        /// <summary>
        /// Occurs when [action confirmed event].
        /// </summary>

        public RelayCommand ConfirmCommand => new RelayCommand(x => ConfirmCommand_Execute());
        public RelayCommand CancelCommand => new RelayCommand(x => CancelCommand_Execute());

        private string actionText;
        public string ActionText
        {
            get { return actionText; }
            set
            {
                actionText = value;
                OnPropertyChanged(() => ActionText);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ActionConfirmationDialogViewModel"/> class.
        /// </summary>
        /// <param name="text">Action text to display</param>
        public ConfirmationDialogViewModel(string text, List<object> paramList = null)
        {
            this.ActionText = text;
            paramList = null;
        }

        /// <summary>
        /// Confirms the command.
        /// </summary>
        private void ConfirmCommand_Execute()
        {
            ActionConfirmEvent.Invoke(true);
            DialogHost.CloseDialogCommand.Execute(true, null);
        }

        /// <summary>
        /// Cancels the command.
        /// </summary>
        private void CancelCommand_Execute()
        {
            ActionConfirmEvent.Invoke(false);
            DialogHost.CloseDialogCommand.Execute(false, null);
        }
    }
}
