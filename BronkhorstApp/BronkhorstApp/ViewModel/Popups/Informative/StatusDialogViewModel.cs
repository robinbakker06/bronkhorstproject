﻿using BronkhorstApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstApp.ViewModel.Popups.Informative
{
    /// <summary>
    /// Status dialog. Standard viewmodel .   (Not used).
    /// </summary>
    /// <seealso cref="BronkhorstApp.Helpers.ObservableObject" />
    public class StatusDialogViewModel : ObservableObject
    {
        private string statustext;

        /// <summary>
        /// Gets or sets the status text.
        /// </summary>
        /// <value>
        /// The status text.
        /// </value>
        public string StatusText
        {
            get { return statustext; }
            set
            {
                statustext = value;
                OnPropertyChanged(() => StatusText);
            }
        }

        private bool isbusy;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is busy.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is busy; otherwise, <c>false</c>.
        /// </value>
        public bool IsBusy
        {
            get { return isbusy; }
            set
            {
                isbusy = value;
                OnPropertyChanged(() => IsBusy);
            }
        }

        private bool isdone;
        /// <summary>
        /// Gets or sets a value indicating whether this instance is done.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is done; otherwise, <c>false</c>.
        /// </value>
        public bool IsDone
        {
            get { return isdone; }
            set
            {

                isdone = value;
                OnPropertyChanged(() => IsDone);
            }
        }

        private bool issuccess;
        /// <summary>
        /// Gets or sets a value indicating whether this instance is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is success; otherwise, <c>false</c>.
        /// </value>
        public bool IsSuccess
        {
            get { return issuccess; }
            set
            {
                issuccess = value;
                if (value)
                {
                    IsDone = true;
                    IsBusy = false;
                }

                OnPropertyChanged(() => IsSuccess);
            }
        }

        private bool isfailed;
        /// <summary>
        /// Gets or sets a value indicating whether this instance is failed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is failed; otherwise, <c>false</c>.
        /// </value>
        public bool IsFailed
        {
            get { return isfailed; }
            set
            {
                isfailed = value;
                if (value)
                {
                    IsDone = true;
                    IsBusy = false;
                }
                OnPropertyChanged(() => IsFailed);
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="StatusDialogViewModel"/> class.
        /// </summary>
        /// <param name="isBusy">if set to <c>true</c> [is busy].</param>
        /// <param name="isSuccess">if set to <c>true</c> [is success].</param>
        public StatusDialogViewModel(bool isBusy, bool isSuccess = false)
        {
            if (isBusy)
            {
                IsSuccess = false;
                IsFailed = false;

                IsBusy = true;

                ChangeStatus("Please wait..");
            }
        }

        /// <summary>
        /// Changes the status.
        /// </summary>
        /// <param name="newText">The new text.</param>
        public void ChangeStatus(string newText)
        {
            this.StatusText = newText;
        }

        /// <summary>
        /// Changes the result.
        /// </summary>
        /// <param name="isSuccess">if set to <c>true</c> [is success].</param>
        /// <param name="statusText">The status text.</param>
        public void ChangeResult(bool isSuccess, string statusText = "")
        {
            if (isSuccess)
            {
                this.IsSuccess = true;
                this.IsFailed = false;

                if (statusText != String.Empty)
                {
                    this.StatusText = statusText;
                }
            }
            else
            {
                this.IsSuccess = false;
                this.IsFailed = true;

                if (statusText != String.Empty)
                {
                    this.StatusText = statusText;
                }
            }
        }
    }
}
