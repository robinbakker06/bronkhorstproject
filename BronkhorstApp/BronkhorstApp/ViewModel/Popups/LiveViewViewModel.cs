﻿using BronkhorstApp.Helpers;
using BronkhorstApp.Model;
using BronkhorstApp.ViewModel.Mapping;
using BronkhorstModbusLibrary.Devices;
using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Wpf;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BronkhorstApp.ViewModel.Popups
{

    /// <summary>
    /// Viewmodel for the live view. Handles all UI operations and is part of the MVVM framework.
    /// </summary>
    /// <seealso cref="BronkhorstApp.Helpers.ObservableObject" />
    public class LiveViewViewModel : ObservableObject
    {
        /// <summary>
        /// Location where the CSV files are stored.
        /// </summary>
        private const string CSV_STORE_LOCATION = @"C:\SaxionBronkhorst\DATA\CSV";

        private LiveModel Model { get; set; }

        #region Relaycommands (View -> ViewModel)
        public RelayCommand StartOverCommand => new RelayCommand(x => StartOverCommand_Execute());

        // This is actually the CSV generation command!
        public RelayCommand SaveDatabaseCommand => new RelayCommand(x => SaveDatabaseCommand_Execute());

        public RelayCommand ExitCommand => new RelayCommand(x => ExitCommand_Execute());
        #endregion


        private string flowsp;
        /// <summary>
        /// Flow setpoint where the measure willstart.
        /// </summary>
        public string FlowSetpoint
        {
            get { return flowsp; }
            set
            {

                flowsp = value;
                OnPropertyChanged(() => FlowSetpoint);
            }
        }

        private string endflowsp;
        /// <summary>
        /// THe end flow setpoint where the measurements will stop
        /// </summary>
        public string EndFlowSetpoint
        {
            get { return endflowsp; }
            set
            {

                endflowsp = value;
                OnPropertyChanged(() => EndFlowSetpoint);
            }
        }

        private string measuretime;
        /// <summary>
        /// Total measure time per measurepoint
        /// </summary>
        public string MeasureTime
        {
            get { return measuretime; }
            set
            {

                measuretime = value;
                OnPropertyChanged(() => MeasureTime);
            }
        }

        private string samplerate;
        /// <summary>
        ///  The sample rate per measure point
        /// </summary>
        public string SampleRate
        {
            get { return samplerate; }
            set
            {

                samplerate = value;
                OnPropertyChanged(() => SampleRate);
            }
        }

        private string measurepoints;
        /// <summary>
        /// Measure point amounts for in the graphs/
        /// </summary>
        public string MeasurePoints
        {
            get { return measurepoints; }
            set
            {

                measurepoints = value;
                OnPropertyChanged(() => MeasurePoints);
            }
        }

        private string settlingtime;
        public string SettlingTime
        {
            get { return settlingtime; }
            set
            {

                settlingtime = value;
                OnPropertyChanged(() => SettlingTime);
            }
        }

        private List<BronkhorstDevice> DeviceList { get; set; }

        #region CHART_SENSOR1
        public SeriesCollection SeriesCollection { get; set; }

        private ChartValues<ELIQValue> setpointValuesIQ;
        public ChartValues<ELIQValue> SetpointValuesIQ
        {
            get { return setpointValuesIQ; }
            set
            {
                setpointValuesIQ = value;
                OnPropertyChanged(() => SetpointValuesIQ);
            }
        }


        private string[] timelabels;
        public string[] IQFlowLabels
        {
            get
            {
                return timelabels;
            }
            set
            {
                timelabels = value;
                OnPropertyChanged(() => IQFlowLabels);
            }
        }

        private string[] elflowlabels;
        public string[] ElFlowLabels
        {
            get
            {
                return elflowlabels;
            }
            set
            {
                elflowlabels = value;
                OnPropertyChanged(() => ElFlowLabels);
            }
        }

        private Func<double, string> setpointformatter;
        public Func<double, string> SetpointFormatter
        {
            get { return setpointformatter; }
            set
            {
                setpointformatter = value;
                OnPropertyChanged(() => SetpointFormatter);
            }
        }

        public string[] Labels { get; set; }

        public Func<double, string> YFormatter { get; set; }
        public Func<double, string> XFormatter { get; set; }

        #endregion

        #region CHART_SENSOR2
        public SeriesCollection SeriesCollection2 { get; set; }

        public ChartValues<ELIQValue> SetpointValues2IQ
        {
            get; set;
        }


        private string[] timelabels2;
        public string[] IQFlowLabels2
        {
            get
            {
                return timelabels2;
            }
            set
            {
                timelabels2 = value;
                OnPropertyChanged(() => IQFlowLabels2);
            }
        }

        private string[] elflowlabels2;
        public string[] ElFlowLabels2
        {
            get
            {
                return elflowlabels2;
            }
            set
            {
                elflowlabels2 = value;
                OnPropertyChanged(() => ElFlowLabels2);
            }
        }

        private Func<double, string> setpointformatter2;
        public Func<double, string> SetpointFormatter2
        {
            get { return setpointformatter2; }
            set
            {
                setpointformatter2 = value;
                OnPropertyChanged(() => SetpointFormatter2);
            }
        }

        public string[] Labels2 { get; set; }

        public Func<double, string> YFormatter2 { get; set; }
        public Func<double, string> XFormatter2 { get; set; }

        #endregion

        #region CHART_SENSOR3
        public SeriesCollection SeriesCollection3 { get; set; }

        private ChartValues<ELIQValue> setpointValues3IQ;
        public ChartValues<ELIQValue> SetpointValues3IQ
        {
            get
            {
                return setpointValues3IQ;
            }
            set
            {
                setpointValues3IQ = value;
                OnPropertyChanged(() => SetpointValues3IQ);
            }
        }

        private string[] timelabels3;
        public string[] IQFlowLabels3
        {
            get
            {
                return timelabels3;
            }
            set
            {
                timelabels3 = value;
                OnPropertyChanged(() => IQFlowLabels3);
            }
        }

        private string[] elflowlabels3;
        public string[] ElFlowLabels3
        {
            get
            {
                return elflowlabels3;
            }
            set
            {
                elflowlabels3 = value;
                OnPropertyChanged(() => ElFlowLabels3);
            }
        }

        private Func<double, string> setpointformatter3;
        public Func<double, string> SetpointFormatter3
        {
            get { return setpointformatter3; }
            set
            {
                setpointformatter3 = value;
                OnPropertyChanged(() => SetpointFormatter3);
            }
        }

        public string[] Labels3 { get; set; }

        public Func<double, string> YFormatter3 { get; set; }
        public Func<double, string> XFormatter3 { get; set; }

        #endregion

        #region CHART_SENSOR4
        public SeriesCollection SeriesCollection4 { get; set; }

        private ChartValues<ELIQValue> setpointValues4IQ;
        public ChartValues<ELIQValue> SetpointValues4IQ
        {
            get
            {
                return setpointValues4IQ;
            }
            set
            {
                setpointValues4IQ = value;
                OnPropertyChanged(() => SetpointValues4IQ);
            }
        }

        private string[] timelabels4;
        public string[] IQFlowLabels4
        {
            get
            {
                return timelabels4;
            }
            set
            {
                timelabels4 = value;
                OnPropertyChanged(() => IQFlowLabels4);
            }
        }

        private string[] elflowlabels4;
        public string[] ElFlowLabels4
        {
            get
            {
                return elflowlabels4;
            }
            set
            {
                elflowlabels4 = value;
                OnPropertyChanged(() => ElFlowLabels4);
            }
        }

        private Func<double, string> setpointformatter4;
        public Func<double, string> SetpointFormatter4
        {
            get { return setpointformatter4; }
            set
            {
                setpointformatter4 = value;
                OnPropertyChanged(() => SetpointFormatter4);
            }
        }

        public string[] Labels4 { get; set; }

        public Func<double, string> YFormatter4 { get; set; }
        public Func<double, string> XFormatter4 { get; set; }

        #endregion

        #region CHART_SENSOR5
        private SeriesCollection seriesCollection5;
        public SeriesCollection SeriesCollection5
        {
            get { return seriesCollection5; }
            set
            {
                seriesCollection5 = value;
                OnPropertyChanged(() => SeriesCollection5);
            }
        }

        private ChartValues<ELIQValue> setpointValues5IQ;
        public ChartValues<ELIQValue> SetpointValues5IQ
        {
            get
            {
                return setpointValues5IQ;
            }
            set
            {
                setpointValues5IQ = value;
                OnPropertyChanged(() => SetpointValues5IQ);
            }
        }

        private string[] timelabels5;
        public string[] IQFlowLabels5
        {
            get
            {
                return timelabels5;
            }
            set
            {
                timelabels5 = value;
                OnPropertyChanged(() => IQFlowLabels5);
            }
        }

        private string[] elflowlabels5;
        public string[] ElFlowLabels5
        {
            get
            {
                return elflowlabels5;
            }
            set
            {
                elflowlabels5 = value;
                OnPropertyChanged(() => ElFlowLabels5);
            }
        }

        private Func<double, string> setpointformatter5;
        public Func<double, string> SetpointFormatter5
        {
            get { return setpointformatter5; }
            set
            {
                setpointformatter5 = value;
                OnPropertyChanged(() => SetpointFormatter5);
            }
        }

        public string[] Labels5 { get; set; }

        public Func<double, string> YFormatter5 { get; set; }
        public Func<double, string> XFormatter5 { get; set; }

        #endregion

        #region CHART_SENSOR6
        public SeriesCollection SeriesCollection6 { get; set; }

        private ChartValues<ELIQValue> setpointValues6IQ;
        public ChartValues<ELIQValue> SetpointValues6IQ
        {
            get
            {
                return setpointValues6IQ;
            }
            set
            {
                setpointValues6IQ = value;
                OnPropertyChanged(() => SetpointValues6IQ);
            }
        }

        private string[] timelabels6;
        public string[] IQFlowLabels6
        {
            get
            {
                return timelabels6;
            }
            set
            {
                timelabels6 = value;
                OnPropertyChanged(() => IQFlowLabels6);
            }
        }

        private string[] elflowlabels6;
        public string[] ElFlowLabels6
        {
            get
            {
                return elflowlabels6;
            }
            set
            {
                elflowlabels6 = value;
                OnPropertyChanged(() => ElFlowLabels6);
            }
        }

        private Func<double, string> setpointformatter6;
        public Func<double, string> SetpointFormatter6
        {
            get { return setpointformatter6; }
            set
            {
                setpointformatter6 = value;
                OnPropertyChanged(() => SetpointFormatter6);
            }
        }

        public string[] Labels6 { get; set; }

        public Func<double, string> YFormatter6 { get; set; }
        public Func<double, string> XFormatter6 { get; set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of LiveViewModel. Initialises all graphs and Variables.
        /// </summary>
        /// <param name="flowsp"></param>
        /// <param name="endflowsp"></param>
        /// <param name="measuretime"></param>
        /// <param name="samplerate"></param>
        /// <param name="MeasurePoints"></param>
        /// <param name="settlingTime"></param>
        /// <param name="devices"></param>
        public LiveViewViewModel(string flowsp, string endflowsp, string measuretime, string samplerate, string MeasurePoints, string settlingTime, List<BronkhorstDevice> devices)
        {
            this.Model = new LiveModel();
            this.DeviceList = devices;
            this.FlowSetpoint = flowsp;
            this.MeasureTime = measuretime;
            this.SampleRate = samplerate;
            this.MeasurePoints = MeasurePoints;
            this.SettlingTime = settlingTime;
            this.EndFlowSetpoint = endflowsp;
            
            // Beneath here is the initialisation for the sensors in the graph. Each graph has a mapper where we map a class
            // to the graph. It defines what values are displayed on the X Axis and Y Axis.
            // We plot the index of the  ChartValues (list) in the X axis, because the first value of the ChartValues list
            // is the first measured value, and the last is the last measured value. 

            #region Initialisation sensor 1
            SetpointValuesIQ = new ChartValues<ELIQValue>();

            var config = Mappers.Xy<ELIQValue>()
              .X((x, index) => index)
              .Y(y => (double)y.IQFlowValue);

            SeriesCollection = new SeriesCollection(config)
                {
                    new LineSeries
                    {
                        Title = "Sensor " + devices[0].DBID,
                        Values = SetpointValuesIQ,
                        ScalesYAt = 0,
                        PointGeometry = null
                    }
                };

            YFormatter = v => (v.ToString("0.00") + " ml/min");
            XFormatter = v => (v.ToString("0.00") + " ml/min");
            #endregion

            #region Initialisation sensor 2
            SetpointValues2IQ = new ChartValues<ELIQValue>();

            var config2 = Mappers.Xy<ELIQValue>()
              .X((x, index) => index)
              .Y(y => (double)y.IQFlowValue);

            SeriesCollection2 = new SeriesCollection(config2)
                {
                    new LineSeries
                    {
                        Title = "Sensor " + devices[1].DBID,
                        Values = SetpointValues2IQ,
                        ScalesYAt = 0,
                        PointGeometry = null
                    }
                };

            YFormatter2 = v => (v.ToString("0.00") + " ml/min");
            XFormatter2 = v => (v.ToString("0.00") + "  ml/min");
            #endregion

            #region Initialisation sensor 3
            SetpointValues3IQ = new ChartValues<ELIQValue>();


            var config3 = Mappers.Xy<ELIQValue>()
              .X((x, index) => index)
              .Y(y => (double)y.IQFlowValue);


            SeriesCollection3 = new SeriesCollection(config3)
                {
                    new LineSeries
                    {
                        Title = "Sensor " + devices[2].DBID,
                        Values = SetpointValues3IQ,
                        ScalesYAt = 0,
                        PointGeometry = null
                    }
                };

            YFormatter3 = v => (v.ToString("0.00") + " ml/min");
            XFormatter3 = v => (v.ToString("0.00") + " ml/min");
            #endregion

            #region Initialisation sensor 4
            SetpointValues4IQ = new ChartValues<ELIQValue>();

            var config4 = Mappers.Xy<ELIQValue>()
              .X((x, index) => index)
              .Y(y => (double)y.IQFlowValue);

            SeriesCollection4 = new SeriesCollection(config4)
                {
                    new LineSeries
                    {
                        Title = "Sensor " + devices[3].DBID,
                        Values = SetpointValues4IQ,
                        ScalesYAt = 0,
                        PointGeometry = null
                    }
                };

            YFormatter4 = v => (v.ToString("0.00") + " ml/min");
            XFormatter4 = v => (v.ToString("0.00") + " ml/min");
            #endregion

            #region Initialisation sensor 5
            SetpointValues5IQ = new ChartValues<ELIQValue>();

            var config5 = Mappers.Xy<ELIQValue>()
               .X((x, index) => index)
               .Y(y => (double)y.IQFlowValue);


            SeriesCollection5 = new SeriesCollection(config5)
                {
                    new LineSeries
                    {
                        Title = "Sensor " + devices[4].DBID,
                        Values = SetpointValues5IQ,
                        ScalesYAt = 0,
                        PointGeometry = null
                    }
                };

            YFormatter5 = v => (v.ToString("0.00") + " ml/min");
            XFormatter5 = v => (v.ToString("0.00") + " ml/min");
            #endregion

            #region Initialisation sensor 6
            SetpointValues6IQ = new ChartValues<ELIQValue>();


            var config6 = Mappers.Xy<ELIQValue>()
               .X((x, index) => index)
               .Y(y => (double)y.IQFlowValue);

            SeriesCollection6 = new SeriesCollection(config6)
                {
                    new LineSeries
                    {
                        Title = "Sensor " + devices[5].DBID,
                        Values = SetpointValues6IQ,
                        ScalesYAt = 0,
                        PointGeometry = null
                    }
                };

            YFormatter6 = v => (v.ToString("0.00") + " ml/min");
            XFormatter6 = v => (v.ToString("0.00") + " ml/min");
            #endregion

        }

        #region Update axis Chart 

        /// <summary>
        /// Starts the measuring process.
        /// </summary>
        private async void StartOverCommand_Execute()
        {
            // Add callback to measurepoint to add a point to the graph.
            Model.MeasureReceivedEvent += MeasurePointCallBack;

            // Start a task to prevent the UI from blocking
            await Task.Run(() => Model.StartMeasure(double.Parse(FlowSetpoint), double.Parse(EndFlowSetpoint), double.Parse(SettlingTime), DeviceList,
                Convert.ToInt32(MeasureTime), Convert.ToInt32(SampleRate), Convert.ToInt32(MeasurePoints)));
        }

        /// <summary>
        /// Export to CSV.
        /// </summary>
        private void SaveDatabaseCommand_Execute()
        {
            ToCSV();
        }

        private void ExitCommand_Execute()
        {
            LiveModel.MeasureTokenSource.Cancel();    // Does not work.
        }

        /// <summary>
        /// Converts the measured values to the CSV.
        /// </summary>
        private void ToCSV()
        {
            string CSVTxt = "";

            // Add all necessary columns
            CSVTxt += "Setpoint (mls/min),EL-Flow(mls/min),IQ+Flow ID:" + DeviceList[0].DBID +"(mls/min)" +
                ",IQ+Flow ID:" + DeviceList[1].DBID + "(mls/min)"
                + ",IQ+Flow ID:" + DeviceList[2].DBID + "(mls/min)"
                + ",IQ+Flow ID:" + DeviceList[3].DBID + "(mls/min)"
                + ",IQ+Flow ID:" + DeviceList[4].DBID + "(mls/min)"
                + ",IQ+Flow ID:" + DeviceList[5].DBID +"(mls/min)";

            CSVTxt += Environment.NewLine;

            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";


            double previousSetpoint = 0.1;
            for (int indexValue = 0; indexValue < SetpointValuesIQ.Count; indexValue++)
            {
                // If the value is the same as the previous one then skip. Because we dont need a row for each sample.
                // If the samplerate is 5 it takes the AVERAGE of 5 samples.
                if (previousSetpoint == SetpointValuesIQ[indexValue].ELFlowSetpoint)
                {
                    continue;
                }
                else
                {
                    previousSetpoint = SetpointValuesIQ[indexValue].ELFlowSetpoint;

                    // Get every measurement for the current setpoint for each IQFlow
                    List<ELIQValue> iq1 = setpointValuesIQ.Where(x => x.ELFlowSetpoint == previousSetpoint).ToList();
                    List<ELIQValue> iq2 = SetpointValues2IQ.Where(x => x.ELFlowSetpoint == previousSetpoint).ToList();
                    List<ELIQValue> iq3 = SetpointValues3IQ.Where(x => x.ELFlowSetpoint == previousSetpoint).ToList();
                    List<ELIQValue> iq4 = SetpointValues4IQ.Where(x => x.ELFlowSetpoint == previousSetpoint).ToList();
                    List<ELIQValue> iq5 = SetpointValues5IQ.Where(x => x.ELFlowSetpoint == previousSetpoint).ToList();
                    List<ELIQValue> iq6 = SetpointValues6IQ.Where(x => x.ELFlowSetpoint == previousSetpoint).ToList();

                    // Add the EL Flow setpoint and the actual measured value from the EL Flow (and the time from start!)
                    CSVTxt += iq1[0].ELFlowSetpoint.ToString(nfi).Replace(",", ".") + ",";
                    CSVTxt += iq1[0].ELFlowValue.ToString(nfi).Replace(",", ".") + "@+" + (iq1[0].MSecFromStart / 1000).ToString(nfi) + ",";


                    // Add the IQ Flow measured values.
                    if (iq1.Count > 1)
                        CSVTxt += iq1.Average(x => x.IQFlowValue).ToString(nfi).Replace(",", ".") + "@+" + (iq1[0].MSecFromStart / 1000).ToString(nfi) + ",";
                    else
                        CSVTxt += iq1[0].IQFlowValue.ToString(nfi).Replace(",", ".") + "@+" + (iq1[0].MSecFromStart / 1000).ToString(nfi) + ",";

                    if (iq2.Count > 1)
                        CSVTxt += iq2.Average(x => x.IQFlowValue).ToString(nfi).Replace(",", ".") + "@+" + (iq2[0].MSecFromStart / 1000).ToString(nfi) + ",";
                    else
                        CSVTxt += iq2[0].IQFlowValue.ToString(nfi).Replace(",", ".") + "@+" + (iq2[0].MSecFromStart / 1000).ToString(nfi) + ",";

                    if (iq3.Count > 1)                                    
                        CSVTxt += iq3.Average(x => x.IQFlowValue).ToString(nfi).Replace(",", ".") + "@+" + (iq3[0].MSecFromStart / 1000).ToString(nfi) + ",";
                    else
                        CSVTxt += iq3[0].IQFlowValue.ToString(nfi).Replace(",", ".") + "@+" + (iq3[0].MSecFromStart / 1000).ToString(nfi) + ",";

                    if (iq4.Count > 1)                                     
                        CSVTxt += iq4.Average(x => x.IQFlowValue).ToString(nfi).Replace(",", ".") + "@+" + (iq4[0].MSecFromStart / 1000).ToString(nfi) + ",";
                    else
                        CSVTxt += iq4[0].IQFlowValue.ToString(nfi).Replace(",", ".") + "@+" + (iq4[0].MSecFromStart / 1000).ToString(nfi) + ",";

                    if (iq5.Count > 1)
                        CSVTxt += iq5.Average(x => x.IQFlowValue).ToString(nfi).Replace(",", ".") + "@+" + (iq5[0].MSecFromStart / 1000).ToString(nfi) + ",";
                    else
                        CSVTxt += iq5[0].IQFlowValue.ToString(nfi).Replace(",", ".") + "@+" + (iq5[0].MSecFromStart / 1000).ToString(nfi) + ",";            

                    if (iq6.Count > 1)
                        CSVTxt += iq6.Average(x => x.IQFlowValue).ToString(nfi).Replace(",", ".") + "@+" + (iq6[0].MSecFromStart / 1000).ToString(nfi) + ",";
                    else
                        CSVTxt += iq6[0].IQFlowValue.ToString(nfi).Replace(",", ".") + "@+" + (iq6[0].MSecFromStart / 1000).ToString(nfi) + ",";

                    CSVTxt += Environment.NewLine;


                }
            }

            try
            {
                // Create a CSV file.

                string csvName = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + "_MeasuredValuesUID" + DateTime.Now.Ticks + ".csv";

                if (!Directory.Exists(CSV_STORE_LOCATION))
                {
                    Directory.CreateDirectory(CSV_STORE_LOCATION);
                }

                // We use a 'using' the make sure the resources are not busy when we append all CSV text to it.
                using (StreamWriter sw = File.CreateText(CSV_STORE_LOCATION + "/" + csvName))
                {

                }
                File.AppendAllText(CSV_STORE_LOCATION + "/" + csvName, CSVTxt);

                // Let the user know it succeeded.
                MessageBox.Show("Succesfully created the CSV File. Location and name: " + Environment.NewLine + CSV_STORE_LOCATION + "/" + csvName);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Something went wrong while creating the CSV. Error: " + ex.Message);
            }


        }

        /// <summary>
        /// Callback for the measurepoint. Adds the measurepoint to the graph. The graphs per sensors are HARDCODED.
        /// </summary>
        /// <param name="valueSensor">Value from the sensor</param>
        /// <param name="totalms">Time in MS from stawrt</param>
        /// <param name="valueElFlow">EL Flow measured value</param>
        /// <param name="sensorId">the SensorID  from Modbus (UnitIdentifier)</param>
        /// <param name="ElFlowSetpoint">Current setpoint for the EL Flow. Calculated with stepheight.</param>
        public void MeasurePointCallBack(object valueSensor, double totalms,
                                         object valueElFlow, int sensorId, double ElFlowSetpoint)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Type typeVal = valueSensor.GetType();

                // In each IF statement for a sensor ID the labels for the EL Flow gets added. This is because we have the X axis based on index.
                // The Index doesn't display a value. That's why there is a El Flow label, the label get's shown. Afterwards the ELIQValue gets made and
                // added to the ChartValues. This is the actual data for the graph. and this gets used for the CSV generation.

                if (typeVal == typeof(float))
                {
                    decimal dec = new decimal(((float)valueSensor));
                    double elFlowValue = Convert.ToDouble(valueElFlow.ToString());

                    if (sensorId == 2 || sensorId == 3)
                    {
                        if (sensorId == 2)
                        {
                         
                            // El FLow  
                            List<string> tempLabels;
                            if (ElFlowLabels != null)
                            {
                                tempLabels = ElFlowLabels.ToList();
                            }
                            else
                            {
                                tempLabels = new List<string>();
                            }

                            tempLabels.Add(valueElFlow.ToString());

                            ElFlowLabels = tempLabels.ToArray();


                            ELIQValue eliq = new ELIQValue();
                            eliq.MSecFromStart = totalms;
                            eliq.ELFlowValue = (double)elFlowValue;
                            eliq.ELFlowSetpoint = ElFlowSetpoint;
                            eliq.SensorDBID = Convert.ToInt16(DeviceList[0].DBID);
                            eliq.IQFlowValue = (double)dec;
                            SetpointValuesIQ.Add(eliq);
                        }
                        else if (sensorId == 3)
                        {
                            // El FLow  
                            List<string> tempLabels;
                            if (ElFlowLabels2 != null)
                            {
                                tempLabels = ElFlowLabels2.ToList();
                            }
                            else
                            {
                                tempLabels = new List<string>();
                            }

                            tempLabels.Add(valueElFlow.ToString());

                            ElFlowLabels2 = tempLabels.ToArray();


                            ELIQValue eliq = new ELIQValue();
                            eliq.MSecFromStart = totalms;
                            eliq.ELFlowSetpoint = ElFlowSetpoint;
                            eliq.ELFlowValue = (double)elFlowValue;
                            eliq.IQFlowValue = (double)dec;
                            eliq.SensorDBID = Convert.ToInt16(DeviceList[1].DBID);

                            SetpointValues2IQ.Add(eliq);
                        }
                    }
                    else if (sensorId == 4 || sensorId == 5)
                    {

                        if (sensorId == 4)
                        {
                            // El FLow  
                            List<string> tempLabels;
                            if (ElFlowLabels3 != null)
                            {
                                tempLabels = ElFlowLabels3.ToList();
                            }
                            else
                            {
                                tempLabels = new List<string>();
                            }

                            tempLabels.Add(valueElFlow.ToString());

                            ElFlowLabels3 = tempLabels.ToArray();


                            ELIQValue eliq = new ELIQValue();
                            eliq.MSecFromStart = totalms;
                            eliq.ELFlowSetpoint = ElFlowSetpoint;
                            eliq.ELFlowValue = (double)elFlowValue;
                            eliq.SensorDBID = Convert.ToInt16(DeviceList[2].DBID);
                            eliq.IQFlowValue = (double)dec;

                            SetpointValues3IQ.Add(eliq);
                        }
                        else if (sensorId == 5)
                        {
                            // El FLow  
                            List<string> tempLabels;
                            if (ElFlowLabels4 != null)
                            {
                                tempLabels = ElFlowLabels4.ToList();
                            }
                            else
                            {
                                tempLabels = new List<string>();
                            }

                            tempLabels.Add(valueElFlow.ToString());

                            ElFlowLabels4 = tempLabels.ToArray();

                            ELIQValue eliq = new ELIQValue();
                            eliq.ELFlowValue = (double)elFlowValue;
                            eliq.ELFlowSetpoint = ElFlowSetpoint;
                            eliq.SensorDBID = Convert.ToInt16(DeviceList[3].DBID);
                            eliq.IQFlowValue = (double)dec;
                            eliq.MSecFromStart = totalms;


                            SetpointValues4IQ.Add(eliq);
                        }
                    }
                    else if (sensorId == 6 || sensorId == 7)
                    {
                        if (sensorId == 6)
                        {
                            // El FLow  
                            List<string> tempLabels;
                            if (ElFlowLabels5 != null)
                            {
                                tempLabels = ElFlowLabels5.ToList();
                            }
                            else
                            {
                                tempLabels = new List<string>();
                            }

                            tempLabels.Add(valueElFlow.ToString());

                            ElFlowLabels5 = tempLabels.ToArray();

                            ELIQValue eliq = new ELIQValue();
                            eliq.MSecFromStart = totalms;
                            eliq.ELFlowSetpoint = ElFlowSetpoint;
                            eliq.SensorDBID = Convert.ToInt16(DeviceList[4].DBID);
                            eliq.ELFlowValue = (double)elFlowValue;
                            eliq.IQFlowValue = (double)dec;

                            SetpointValues5IQ.Add(eliq);

                        }
                        else if (sensorId == 7)
                        {
                            // El FLow  
                            List<string> tempLabels;
                            if (ElFlowLabels6 != null)
                            {
                                tempLabels = ElFlowLabels6.ToList();
                            }
                            else
                            {
                                tempLabels = new List<string>();
                            }

                            tempLabels.Add(valueElFlow.ToString());

                            ElFlowLabels6 = tempLabels.ToArray();

                            ELIQValue eliq = new ELIQValue();
                            eliq.SensorDBID = Convert.ToInt16(DeviceList[5].DBID);
                            eliq.ELFlowSetpoint = ElFlowSetpoint;
                            eliq.MSecFromStart = totalms;
                            eliq.ELFlowValue = (double)elFlowValue;
                            eliq.IQFlowValue = (double)dec;

                            SetpointValues6IQ.Add(eliq);
                        }
                    }
                }
            });
        }
    }
}
#endregion