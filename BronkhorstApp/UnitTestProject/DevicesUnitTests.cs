﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BronkhorstModbusLibrary.Communication;
using BronkhorstModbusLibrary.Communication.Modbus;
using BronkhorstModbusLibrary.Enumerations;
using BronkhorstModbusLibrary.Devices;
using BronkhorstModbusLibrary.Communication.Modbus.Bronkhorst;
using BronkhorstApp.Model.JSON;

namespace UnitTestProject
{
    [TestClass]
    public class DevicesUnitTests
    {
        [TestMethod]
        public void ConnectDevicesTest()
        {
            // TEST1 : Tests the connection to one IQ Flow module and the EL Flow.

            bool elFlowConnected;
            bool iqFlowConnected;

            ModbusSettings settings = new ModbusSettings("Configuration", 15, 19200, ModbusType.RTU, System.IO.Ports.Parity.Even);

            BronkhorstDevice device;
            BronkhhorstSetup.AddDevice("ELFlow", 1, BronkhorstDeviceType.EL_FLOW,
                settings, out device);

            device.ModbusConnect();
            elFlowConnected = device.IsConnected;

            BronkhorstDevice deviceIqFlow;
            BronkhhorstSetup.AddDevice("IQFlowModule", 2, BronkhorstDeviceType.IQ_FLOW_MODULE,
                settings, out deviceIqFlow);

            deviceIqFlow.ModbusConnect();
            iqFlowConnected = deviceIqFlow.IsConnected;

            Assert.IsTrue((elFlowConnected == true && iqFlowConnected == true));
        }

        [TestMethod]
        public void DeviceConfigurationDataTest()
        {
            // Test: Tests adding devices and configurations and saving them to the disk. Then tests the retrieving of the device and configuration.
            //       Lastly tests the retrieved device and configuration if the program connects. If all are successfully executed the test is completed.
            
            ModbusSettings settings = new ModbusSettings("ConfigurationTEST", 15, 19200, ModbusType.RTU, System.IO.Ports.Parity.Even);

            BronkhorstDevice device;
            BronkhhorstSetup.AddDevice("ELFlowTEST", 1, BronkhorstDeviceType.EL_FLOW,
                settings, out device);

            bool deviceSaved = JSONModel.SaveObject<BronkhorstDevice>(device);
            bool settingsSaved = JSONModel.SaveObject<ModbusSettings>(settings);

            bool settingsRetrieved = false;

            JSONModel.GetAllObjects<ModbusSettings>().ForEach((x) =>
            {
                if (x.Name == settings.Name)
                {
                    settings = x;
                    settingsRetrieved = true;
                }

            });

            bool deviceRetrieved = false;

            JSONModel.GetAllObjects<BronkhorstDevice>().ForEach((x) =>
            {
                if (x.DeviceName == device.DeviceName)
                {
                    device = x;
                    deviceRetrieved = true;
                }

            });

            device.ModbusConnect();

            bool deviceConnected = device.IsConnected;

            JSONModel.RemoveObject<BronkhorstDevice>(device);
            JSONModel.RemoveObject<ModbusSettings>(settings);

            Assert.IsTrue((deviceSaved && settingsSaved && deviceRetrieved && settingsRetrieved && deviceConnected));

        }
    }
}
