﻿using BronkhorstModbusLibrary.Communication.Modbus;
using BronkhorstModbusLibrary.Devices;
using BronkhorstModbusLibrary.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstModbusLibrary.Communication
{

    /// <summary>
    /// Test
    /// </summary>
    public static class BronkhhorstSetup
    {

        /// <summary>
        /// Gets or sets the devices.
        /// </summary>
        /// <value>
        /// The devices.
        /// </value>
        public static List<BronkhorstDevice> Devices { get; set; }

        /// <summary>
        /// Gets or sets the bronkhorst device.
        /// </summary>
        /// <value>
        /// The bronkhorst device.
        /// </value>
        public static BronkhorstDevice BronkhorstDevice
        {
            get => default(BronkhorstDevice);
            set
            {
            }
        }

        /// <summary>
        /// Adds the device to the list of devices.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="type">The type.</param>
        /// <param name="settings">The settings.</param>
        /// <param name="device">The device.</param>
        /// <returns></returns>
        public static bool AddDevice(string name, int id, BronkhorstDeviceType type, ModbusSettings settings, out BronkhorstDevice device)
        {
            device = null;

            if (Devices == null)
                Devices = new List<BronkhorstDevice>();

            BronkhorstDevice dev = new BronkhorstDevice(type, id, name, settings);
            Devices.Add(dev);

            if (Devices.Exists(x => x == dev))
            {
                device = dev;
                return true;
            }
            else
                return false;

        }

        /// <summary>
        /// Adds the device to the devices list
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="type">The type.</param>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        public static bool AddDevice(string name, int id, BronkhorstDeviceType type, ModbusSettings settings)
        {

            if (Devices == null)
                Devices = new List<BronkhorstDevice>();

            BronkhorstDevice dev = new  BronkhorstDevice(type, id, name, settings);
            Devices.Add(dev);

            if (Devices.Exists(x => x == dev))
            {
                return true;
            }
            else
                return false;

        }
    }
}
