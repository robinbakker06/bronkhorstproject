﻿using EasyModbus;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstModbusLibrary.Communication.Modbus
{
    /// <summary>
    /// Modbus communication class. Contains the necessary data in order to setup the connection
    /// to the specified device.
    /// </summary>
    /// <seealso cref="BronkhorstModbusLibrary.Communication.Modbus.ModbusSettings" />
    public class ModbusCommunication : ModbusSettings
    {
        /// <summary>
        /// Gets or sets the modbus client. (Ignored by JSON storage)
        /// </summary>
        /// <value>
        /// The modbus client.
        /// </value>
        [JsonIgnore]
        public static ModbusClient _ModbusClient { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance is connected.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is connected; otherwise, <c>false</c>.
        /// </value>
        public bool IsConnected
        {
            get
            {
                if (_ModbusClient != null)
                    return _ModbusClient.Connected;
                else
                    return false;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModbusCommunication"/> class.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="id">The identifier.</param>
        public ModbusCommunication(ModbusSettings settings, int id) : 
            base(settings.Name, settings.Port, settings.BaudRate, settings.ModbusMode, settings.Parity)
        {
            if(_ModbusClient != null && _ModbusClient.Connected)
            {
                return;
            }

            _ModbusClient = new ModbusClient("COM" + settings.Port);
            _ModbusClient.Baudrate = settings.BaudRate;
            _ModbusClient.UnitIdentifier = (byte)id;
            _ModbusClient.Parity = settings.Parity;

            if(_ModbusClient.LogFileFilename == null)
            {
                _ModbusClient.LogFileFilename = "C:/test/test.txt";
            }
        }

        /// <summary>
        /// Connects to the device using Modbus Protocol
        /// </summary>
        public void ModbusConnect()
        {
            if (_ModbusClient != null && _ModbusClient.Connected)
                return;

            _ModbusClient.Connect();
        }

        /// <summary>
        /// Disconnects the device. 
        /// </summary>
        public void ModbusDisconnect()
        {
            if (_ModbusClient != null && _ModbusClient.Connected)
                _ModbusClient.Disconnect();
        }


    }
}
