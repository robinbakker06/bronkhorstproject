﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstModbusLibrary.Communication.Modbus.Bronkhorst
{
    /// <summary>
    /// Register access (Read, write or ReadWrite)
    /// </summary>
    public enum RegisterAccess
    {
        R,
        W,
        RW
    }
}
