﻿using BronkhorstModbusLibrary.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstModbusLibrary.Communication.Modbus.Bronkhorst
{
    /// <summary>
    /// Static class that contains all available registers.
    /// </summary>
    public static class BronkhorstRegisters
    {
        public static Register[] Registers = new Register[]
        {
           new Register("Wink", typeof(char), 0, RegisterAccess.W, BronkhorstDeviceType.EL_FLOW),
           new Register("Setpoint", typeof(int), 33, RegisterAccess.RW, BronkhorstDeviceType.EL_FLOW),
           new Register("Setpoint slope", typeof(char), 34, RegisterAccess.RW, BronkhorstDeviceType.EL_FLOW),
           new Register("Measure", typeof(char), 32, RegisterAccess.R, BronkhorstDeviceType.EL_FLOW),
           new Register("FMeasure", typeof(float), 41216, 41217, RegisterAccess.R, BronkhorstDeviceType.EL_FLOW),
           new Register("FSetpoint", typeof(float), 41240, 41241, RegisterAccess.RW, BronkhorstDeviceType.EL_FLOW),
           new Register("Capacity Unit", typeof(string), 33272, 33275, RegisterAccess.RW, BronkhorstDeviceType.EL_FLOW),
           new Register("Firmware Version", typeof(string), 61736, 61738, RegisterAccess.R, BronkhorstDeviceType.EL_FLOW),
           new Register("Fluid Name", typeof(string), 33160, 33164, RegisterAccess.RW, BronkhorstDeviceType.EL_FLOW),
           new Register("Serial Number", typeof(string), 61720, 61727, RegisterAccess.RW, BronkhorstDeviceType.EL_FLOW),
           new Register("Temperature", typeof(float), 41272, 41273, RegisterAccess.R, BronkhorstDeviceType.EL_FLOW),
           new Register("Counter Value", typeof(float), 59400, 59401, RegisterAccess.RW, BronkhorstDeviceType.EL_FLOW),
           new Register("Initreset", typeof(char), 10, RegisterAccess.RW, BronkhorstDeviceType.EL_FLOW),
           new Register("Capacity", typeof(float), 33128, 33129, RegisterAccess.RW, BronkhorstDeviceType.EL_FLOW),
           new Register("Capacity 0%", typeof(float), 41392, 41393, RegisterAccess.RW, BronkhorstDeviceType.EL_FLOW),

            new Register("Initreset", typeof(char), 10, RegisterAccess.RW, BronkhorstDeviceType.IQ_FLOW_MODULE),
            new Register("Bridge Potmeter", typeof(char), 3717, RegisterAccess.RW, BronkhorstDeviceType.IQ_FLOW_MODULE),
            new Register("Sensor Type", typeof(char), 64, RegisterAccess.RW, BronkhorstDeviceType.IQ_FLOW_MODULE),
            new Register("Fluid Name", typeof(string), 33160, 33164, RegisterAccess.RW, BronkhorstDeviceType.IQ_FLOW_MODULE),
            new Register("Fluid Capacity", typeof(float), 33128, 33129, RegisterAccess.RW, BronkhorstDeviceType.IQ_FLOW_MODULE),
             new Register("Capacity", typeof(float), 33128, 33129, RegisterAccess.RW, BronkhorstDeviceType.IQ_FLOW_MODULE),
            new Register("Capacity Unit", typeof(string), 33272, 33275, RegisterAccess.RW, BronkhorstDeviceType.IQ_FLOW_MODULE),
            new Register("FMeasure", typeof(float), 41216, 41217, RegisterAccess.R, BronkhorstDeviceType.IQ_FLOW_MODULE),
            new Register("Measure", typeof(char), 32, RegisterAccess.R, BronkhorstDeviceType.IQ_FLOW_MODULE)
        };

        public static Register Register
        {
            get => default(Register);
            set
            {
            }
        }

        /// <summary>
        /// Gets the register.
        /// </summary>
        /// <param name="registername">The registername.</param>
        /// <param name="">The .</param>
        /// <param name="instrumentType">Type of the instrument.</param>
        /// <returns></returns>
        public static Register GetRegister(string registername, BronkhorstDeviceType instrumentType)
        {
            Register retVal = null;

            Registers.ToList().ForEach((x) =>
            {
                // Check if the name is the same (not case sensitive) and if the instrument matches.
                if (x.RegisterName.ToLower() == registername.ToLower() && x.InstrumentCompatibilityType == instrumentType)
                    retVal = x;
            });

            return retVal;
        }
    }
}
