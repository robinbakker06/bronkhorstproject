﻿using BronkhorstModbusLibrary.Enumerations;
using EasyModbus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstModbusLibrary.Communication.Modbus.Bronkhorst
{
    /// <summary>
    /// Register class represents an Modbus Register.
    /// </summary>
    public class Register
    {
        /// <summary>
        /// Gets or sets the name of the register.
        /// </summary>
        /// <value>
        /// The name of the register.
        /// </value>
        public string RegisterName { get; set; }

        /// <summary>
        /// Gets or sets the type of the register.
        /// </summary>
        /// <value>
        /// The type of the register.
        /// </value>
        public Type RegisterType { get; set; }

        /// <summary>
        /// Gets or sets the pdu address start.
        /// </summary>
        /// <value>
        /// The start address of the pdu.
        /// </value>
        public int PDUAddressStart { get; set; }

        /// <summary>
        /// Gets or sets the end pdu address.
        /// </summary>
        /// <value>
        /// The end address of the PDU
        /// </value>
        public int PDUAddressEnd { get; set; }

        /// <summary>
        /// Gets or sets the register access.
        /// </summary>
        /// <value>
        /// The register access.
        /// </value>
        public RegisterAccess Access { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is multiple registers.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is multiple registers; otherwise, <c>false</c>.
        /// </value>
        public bool IsMultipleRegisters { get; set; }

        /// <summary>
        /// Gets or sets the type of the instrument compatibility.
        /// </summary>
        /// <value>
        /// The type of the instrument compatibility.
        /// </value>
        public BronkhorstDeviceType InstrumentCompatibilityType { get; set; }

        /// <summary>
        /// Gets or sets the length of the byte.
        /// </summary>
        /// <value>
        /// The length of the byte.
        /// </value>
        public int ByteLength { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Register"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="type">The type.</param>
        /// <param name="pduaddress">The pduaddress.</param>
        /// <param name="access">The access.</param>
        public Register(string name, Type type, int pduaddress, RegisterAccess access, BronkhorstDeviceType instrument)
        {
            this.RegisterName = name;
            this.RegisterType = type;
            this.PDUAddressStart = pduaddress;
            this.Access = access;
            this.IsMultipleRegisters = false;
            this.InstrumentCompatibilityType = instrument;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Register"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="type">The type.</param>
        /// <param name="pduaddressStart">The pduaddress start.</param>
        /// <param name="pduaddressEnd">The pduaddress end.</param>
        /// <param name="access">The access.</param>
        public Register(string name, Type type, int pduaddressStart, int pduaddressEnd, RegisterAccess access, BronkhorstDeviceType instrument)
        {
            this.PDUAddressStart = pduaddressStart;
            this.PDUAddressEnd = pduaddressEnd;
            this.RegisterName = name;
            this.Access = access;
            this.RegisterType = type;
            this.IsMultipleRegisters = true;
            this.InstrumentCompatibilityType = instrument;
        }

        /// <summary>
        /// Writes the specified modbus instance.
        /// </summary>
        /// <param name="modbus">The modbus instance</param>
        /// <param name="val">The value to write</param>
        /// <returns></returns>
        public bool Write(ModbusClient modbus, object val)
        {
            try
            {
                if (Access == RegisterAccess.RW || Access == RegisterAccess.W)
                {
                    if (modbus.Connected)
                    {
                        switch (IsMultipleRegisters)
                        {
                            case true:
                                modbus.WriteMultipleRegisters(PDUAddressStart, (int[])val);
                                return true;
                            case false:

                                modbus.WriteSingleRegister(PDUAddressStart, ((int[])val)[0]);
                                return true;
                        }
                    }
                }
            }
            catch(Exception ex)
            {

            }

            return false;
        }

        /// <summary>
        /// Reads the specified modbus instance at the specified register PDU address (start or end). Determines 
        /// if the register is multiple registers.
        /// </summary>
        /// <param name="modbus">The modbus instance</param>
        /// <returns></returns>
        public int[] Read(ModbusClient modbus)
        {
            try
            {
                if (Access == RegisterAccess.RW || Access == RegisterAccess.R)
                {
                    if (modbus.Connected)
                    {
                        switch (IsMultipleRegisters)
                        {
                            case true:
                                return modbus.ReadHoldingRegisters(PDUAddressStart, (PDUAddressEnd - PDUAddressStart) + 1);

                            case false:
                                return modbus.ReadHoldingRegisters(PDUAddressStart, 1);

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                // WHen there is a problem, just try again.
                modbus.Disconnect();
                modbus.Connect();
                return Read(modbus);
            }

            return null;
        }
    }
}
