﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstModbusLibrary.Communication.Modbus
{
    /// <summary>
    /// Modbus type enumeration for the protocol. 
    /// </summary>
    public enum ModbusType
    {
        RTU,
        IP
    }
}
