﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstModbusLibrary.Communication.Modbus
{
    public class ModbusSettings
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        /// <value>
        /// The port.
        /// </value>
        public int Port { get; set; }

        /// <summary>
        /// Gets or sets the baud rate.
        /// </summary>
        /// <value>
        /// The baud rate.
        /// </value>
        public int BaudRate { get; set; }

        /// <summary>
        /// Gets or sets the modbus mode.
        /// </summary>
        /// <value>
        /// The modbus mode.
        /// </value>
        public ModbusType ModbusMode { get; set; }

        /// <summary>
        /// Gets or sets the parity.
        /// </summary>
        /// <value>
        /// The parity.
        /// </value>
        public Parity Parity { get; set; }

        /// <summary>
        /// Gets or sets the type of the modbus protocol (IP or RTU).
        /// </summary>
        /// <value>
        /// The type of the modbus protocol.
        /// </value>
        public ModbusType ModbusType
        {
            get => default(ModbusType);
            set
            {
            }
        }

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModbusSettings"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="port">The port.</param>
        /// <param name="baudrate">The baudrate.</param>
        /// <param name="type">The modbus protocol type. (RTU or TCP)</param>
        /// <param name="parity">The parity.</param>
        public ModbusSettings(string name, int port, int baudrate, ModbusType type, Parity parity)
        {
            this.Name = name;
            this.Port = port;
            this.BaudRate = baudrate;
            this.ModbusMode = type;
            this.Parity = parity;
        }
    }
}
