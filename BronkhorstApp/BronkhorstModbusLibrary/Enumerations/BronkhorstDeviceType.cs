﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronkhorstModbusLibrary.Enumerations
{
    /// <summary>
    /// Device type enumeration. Contains all supported bronkhorst devices. When adding
    /// devices to this enumeration make sure to implement the support for the compatibility.
    /// </summary>
    public enum BronkhorstDeviceType
    {
        EL_FLOW,
        IQ_FLOW_MODULE,
        EL_PRESS
    }
}
