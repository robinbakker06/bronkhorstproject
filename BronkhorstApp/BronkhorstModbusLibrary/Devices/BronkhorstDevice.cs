﻿using BronkhorstModbusLibrary.Communication.Modbus;
using BronkhorstModbusLibrary.Communication.Modbus.Bronkhorst;
using BronkhorstModbusLibrary.Enumerations;
using EasyModbus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BronkhorstModbusLibrary.Devices
{
    /// <summary>
    /// Bronkhorst device class that used ModbusCommunication to read or write to a device.
    /// </summary>
    /// <seealso cref="BronkhorstModbusLibrary.Communication.Modbus.ModbusCommunication" />
    public class BronkhorstDevice : ModbusCommunication
    {
        /// <summary>
        /// Gets or sets the type of the device.
        /// </summary>
        /// <value>
        /// The type of the device.
        /// </value>
        public BronkhorstDeviceType DeviceType { get; set; }

        /// <summary>
        /// Gets or sets the unit identifier. (DEPRECATED)
        /// </summary>
        /// <value>
        /// The unit identifier.
        /// </value>
        public int UnitIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the name of the device.
        /// </summary>
        /// <value>
        /// The name of the device.
        /// </value>
        public string DeviceName { get; set; }

        /// <summary>
        /// Gets or sets the Database ID of the device.  (Configured by user!)   !! -1 if not configured !!
        /// </summary>
        /// <value>
        /// The Database ID of the device.
        /// </value>
        public int DBID { get; set; }

        /// <summary>
        /// Gets or sets the settings.
        /// </summary>
        /// <value>
        /// The settings.
        /// </value>
        public ModbusSettings Settings { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BronkhorstDevice"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="UnitIdentifier">The unit identifier.</param>
        /// <param name="DeviceName">Name of the device.</param>
        /// <param name="Settings">The settings.</param>
        public BronkhorstDevice(BronkhorstDeviceType type, int UnitIdentifier, string DeviceName, ModbusSettings Settings) : base(Settings, UnitIdentifier)
        {
            this.UnitIdentifier = UnitIdentifier;
            this.DeviceType = type;
            this.DeviceName = DeviceName;
            this.Settings = Settings;
            DBID =  -1;
        }

        /// <summary>
        /// Reads the register value and converts automatically to the correct datatype.
        /// </summary>
        /// <param name="reg">The register to read</param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException">Read conversion expected non null. Expected converted value</exception>
        public object ReadRegister(Register reg)
        {
            // Check if the modbus client is initialized.
            if(ModbusCommunication._ModbusClient == null)
            {
                throw new NullReferenceException("_ModbusClient is NULL. cannot change ID");
            }

            // !!!!!!!!!!!!!!!!! Change the unit identifier. This is really important as this addresses the IQ+FLow or ELFlow that we need to connect to.
            // Otherwise it could connect with the wrong device and this really messes up things.
            ModbusCommunication._ModbusClient.UnitIdentifier = (byte)this.UnitIdentifier;

            int[] retVal = reg.Read(ModbusCommunication._ModbusClient);

            object convValue = null;

            var @switch = new Dictionary<Type, Action> {
                    {
                        typeof(float), () =>
                        {
                            convValue = ModbusClient.ConvertRegistersToFloat(retVal, ModbusClient.RegisterOrder.HighLow);
                        }
                    },
                    {
                        typeof(char), () =>
                        {
                            convValue = retVal[0];
                        }
                    },
                    {
                        typeof(int), () =>
                        {
                            // convValue = ModbusClient.ConvertRegistersToInt(retVal);

                            // Convert to Int. (HighLow registers).
                            int highRegister = retVal[0];
                            byte[] highRegisterBytes = BitConverter.GetBytes(highRegister);
                            byte[] doubleBytes = {
                                                    highRegisterBytes[0],
                                                    highRegisterBytes[1]
                                                };
                            convValue = BitConverter.ToInt16(doubleBytes, 0);
                        }
                    },
                    {
                        typeof(string), () =>
                        {
                             // Convert the read register values to a string. 

                              byte[] result = new byte[16];
                              byte[] registerResult = new byte[2];

                                for (int i = 0; i < result.Length/2; i++)
                                {
                                    if(i < retVal.Length)
                                    {
                                        // Get the bytes from the current register.
                                        registerResult = BitConverter.GetBytes(retVal[0 + i]);

                                        result[i * 2] = registerResult[1];
                                        result[i * 2 + 1] = registerResult[0];
                                    }
                                }

                                // Retrieve the string. (with blank spaces)
                                string test = System.Text.Encoding.Default.GetString(result);

                                char[] chars = test.ToCharArray();
                                string trimmedString = "";

                                // Add all characthers to a trimmed string.
                                foreach(char c in chars.ToList())
                                {
                                    if(c != 0)
                                    {
                                       trimmedString += c;
                                    }
                                }

                                // Remove all whitespaces to form aproper string.
                               convValue = new string(trimmedString.ToCharArray()
                                .Where(c => !Char.IsWhiteSpace(c))
                                .ToArray());
                        }
                    }
                };

            Type swType = reg.RegisterType;
            @switch[swType]();

            if (convValue == null)
            {
                throw new NullReferenceException("Read conversion expected non null. Expected converted value");
            }
            return convValue;
        }

        /// <summary>
        /// Writes the register. The value gets converted into the correct datatype automatically.
        /// </summary>
        /// <param name="reg">The register to write to</param>
        /// <param name="value">The value to write to the register.</param>
        /// <exception cref="NullReferenceException">Converted value is NULL. expected type conversion</exception>
        public void WriteRegister(Register reg, object value)
        {
            if (ModbusCommunication._ModbusClient == null)
            {
                throw new NullReferenceException("_ModbusClient is NULL. cannot change ID");
            }

            // !!!!!!!!!!!!!!!!! Change the unit identifier. This is really important as this addresses the IQ+FLow or ELFlow that we need to connect to.
            // Otherwise it could connect with the wrong device and this really messes up things.
            ModbusCommunication._ModbusClient.UnitIdentifier = (byte)this.UnitIdentifier;

            object convValue = null;

            var @switch = new Dictionary<Type, Action> {
                    {
                        typeof(float), () =>
                        {

                            // Prepare the registers for the selected bronkhorstdevice.
                            byte[] floatBytes = BitConverter.GetBytes((float)value);

                            // High part goes first.
                            byte[] registers =
                            {
                                floatBytes[2],
                                floatBytes[3],
                                0,
                                0
                            };

                            // Low parts go seconds..
                            byte[] _registers =
                            {
                                floatBytes[0],
                                floatBytes[1],
                                0,
                                0
                            };

                            // Combine them

                            int[] values =
                            {
                                BitConverter.ToInt16(registers, 0),
                                BitConverter.ToInt16(_registers, 0)
                            };

                            convValue = values;
                        }
                    },
                    {
                        typeof(int), () =>
                        {
                             convValue = ModbusClient.ConvertIntToRegisters((int)value);
                        }
                    },
                    {
                        typeof(char), () =>
                        {
                             convValue = ModbusClient.ConvertIntToRegisters((int)value);
                        }
                    },
                    {
                        typeof(string), () =>
                        {
                            // Calculate the amount of registers needed..
                            int totalRegisters = (reg.PDUAddressEnd - reg.PDUAddressStart) + 1;

                            //Get all characters.
                            char[] characters = value.ToString().ToCharArray();

                            //Determine amount of characters that fit into the registers
                            int totalCharacters = totalRegisters * 2;

                            if(characters.Length <= totalCharacters)
                            {
                                int[] registerArray = new int[totalRegisters];

                                // For each character that can still fit into the registers add a space.
                                int amountCharactersLeft = (totalCharacters - characters.Length);
                                for(int i = 0; i< amountCharactersLeft; i++)
                                {
                                    value += " ";
                                }

                                byte[] stringByteArray = System.Text.Encoding.ASCII.GetBytes(value.ToString());
                                
                                // Fill the register array.
                                for (int i = 0; i < registerArray.Length;  i++)
                                {
                                    if(i*2 + 1 <= stringByteArray.Length)
                                    {
                                            registerArray[i] = stringByteArray[i * 2 + 1];
                                            if (i*2 < stringByteArray.Length)
                                            {
                                                registerArray[i] = registerArray[i] | ((int)stringByteArray[i * 2] << 8);
                                            }
                                    }
                                }
                                convValue = registerArray;
                        }
                    }
                }};

            Type swType = reg.RegisterType;
            @switch[swType]();

            if (convValue == null)
            {
                throw new NullReferenceException("Converted value is NULL. expected type conversion");
            }

            // Write the converted value to the modbus (UnitIdentifier is used here)!

            reg.Write(ModbusCommunication._ModbusClient, convValue);
        }

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return DeviceName;
        }

    }
}
